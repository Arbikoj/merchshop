<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Facades\Cart;

class CartIcon extends Component
{
    public $cartTotal = 0;
    
    protected $listeners = [
        'cartAdded' => 'updateCartTotal',
        'productRemoved' => 'updateCartTotal',
        'clearCart' => 'updateCartTotal',
    ];

    public function mount()
    {
        $this->cartTotal = count(Cart::get()['product']);      
    }

    public function render()
    {
        return view('livewire.cart-icon');
        
        // return view('livewire.cart-icon', [
        //     'products' => $products,
        // ]);
    }

    public function updateCartTotal()
    {
        $this->cartTotal = count(Cart::get()['product']);
    }

}

