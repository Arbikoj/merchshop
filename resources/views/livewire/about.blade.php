
<div>
    <div class="absolute">
        <img src="{{ asset('bg/bg1.png')}}" class="object-cover opacity-80">
    </div>
</div>
    <section class="text-gray-600 body-font">
        <div class="container relative px-5 py-10 mx-auto mt-3">
            <div class="flex flex-col text-center w-full mb-20">
                <h1 class="sm:text-3xl text-2xl font-medium title-font mb-4 text-white">MerchShop</h1>
                <p class="lg:w-2/3 mx-auto leading-relaxed text-white text-base">Hai! Di MerchShop kamu dapat menemukan berbagai macam merchandise menarik yang dijual dari berbagai perguruan tinggi di Indonesia loh!! Feel free to reach us if you have any questions!</p>
            </div>
            <div class="flex flex-wrap -m-2">
                <div class="p-2 lg:w-1/3 md:w-1/2 w-full">
                    <div class="h-full flex items-center bg-white border-gray-200 border p-4 rounded-lg">
                        <img
                            alt="team"
                            class="w-16 h-16 border-indigo-400 border-2 bg-white object-cover object-center flex-shrink-0 rounded-full mr-4"
                            src="{{asset('asset/acaa.svg')}}">
                            <div class="flex-grow">
                                <h2 class="text-gray-900 title-font font-medium">Maulina Salsabila Alzahra</h2>
                                <p class="text-gray-500">3220600002</p>
                            </div>
                        </div>
                    </div>
                    <div class="p-2 lg:w-1/3 md:w-1/2 w-full">
                        <div class="h-full flex bg-white items-center border-gray-200 border p-4 rounded-lg">
                            <img
                                alt="team"
                                class="w-16 border-indigo-400 border-2 h-16 bg-white object-cover object-center flex-shrink-0 rounded-full mr-4"
                                src="{{asset('asset/arbi.svg')}}">
                                <div class="flex-grow">
                                    <h2 class="text-gray-900 title-font font-medium">Dhimas Adrian</h2>
                                    <p class="text-gray-500">3220600011</p>
                                </div>
                            </div>
                        </div>
                        <div class="p-2 lg:w-1/3 md:w-1/2 w-full">
                            <div class="h-full flex bg-white items-center border-gray-200 border p-4 rounded-lg">
                                <img
                                    alt="team"
                                    class="w-16 border-indigo-400 border-2 h-16 bg-white object-cover object-center flex-shrink-0 rounded-full mr-4"
                                    src="{{asset('asset/arbi.svg')}}">
                                    <div class="flex-grow">
                                        <h2 class="text-gray-900 title-font font-medium">Arbi Julianto</h2>
                                        <p class="text-gray-500">3220600024</p>
                                    </div>
                                </div>
                            </div>
                        <img src="{{asset('asset/bg.svg')}}" class="mt-20 object-cover h-30" alt="" srcset="">
                        
            </div>
        </section>
    </div>