<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetailUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('users_id')->constrained();
            $table->string('jenis_kelamin', 10);
            $table->date('tanggal_lahir', $precision = 0);
            $table->string('nama_depan', 225);
            $table->string('nama_belakang', 225);
            $table->timestamp('email_verivication', $precision = 0);
            $table->string('no_telepon', 15);
            $table->string('confirm_password', 225);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_users');
    }
}
