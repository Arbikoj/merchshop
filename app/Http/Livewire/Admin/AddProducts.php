<?php

namespace App\Http\Livewire\Admin;
use Livewire\WithPagination;
use Livewire\Component;

class AddProducts extends Component
{
    public $produk;
    public $produk_id;
    public // $subcategories_id,
        $categories_id,
        $img_products_id,
        $nama_produk,
        $slug,
        $desc_produk,
        $kondisi,
        $harga,
        $diskon,
        $stok,
        $sku,
        $pre_order,
        $release_product;

        //variable search
        public $srcItem;
        protected $queryString = ['srcItem'];
        public $isModal = 0;

        // pagination
        public $paginate;
        use WithPagination;

        // status value 
        public $statusvalue = 0;

        // kategori
        public $dropdownkategori;
    public function render()
    {
        $this->dropdownkategori = category::all();
        $srcItem = '%'.$this->srcItem.'%';
        return view('livewire.admin.products',[
            'katalog' => product::Where('nama_produk','LIKE',$srcItem)
            ->orWhere('desc_produk','LIKE',$srcItem)
            ->orderBy('created_at', 'DESC')->paginate($this->paginate),
        ]);
    }
}
