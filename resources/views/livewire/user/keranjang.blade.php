<div>
    {{-- In work, do what you enjoy. --}}
    <div class="col-md-12">
       
        <div class="card">
            <table class="min-w-full leading-normal">
                <thead>
                    <tr>
                        <th
                            class="w-5  px-2 py-3 justify-center border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                            <span class="mx-auto font-bold justify-center">Opsi</span>
                        </th>
                        <th
                            class="mx-auto px-2 py-3 justify-center border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                            <span class="font-bold justify-center">Nama Produk</span>
                        </th>
                        <th
                            class="mx-auto px-2 py-3 justify-center border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                            <span class="font-bold justify-center">Harga</span>
                        </th>
                        <th
                            class="mx-auto px-2 py-3 justify-center border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                            <span class="font-bold justify-center">Jumlah</span>
                        </th>
                        <th
                            class="mx-auto px-2 py-3 justify-center border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                            <span class="font-bold justify-center">Sub total</span>
                        </th>
                    </tr>
                </thead>
                <tbody>
            @forelse ($cart['product'] as $product)
                    @php
                    $jum_total = 1;
                    
                        $subtotal = $product->harga * $jum_item;

                        $product->nama_produk = Str::substr($product->nama_produk, 0, 20);
                    @endphp
                    <tr>
                        <td class="px-2 py-2 md:px-5 md:py-5 md:text-lg text-md border-b border-gray-200 bg-white">
                            <div class="flex justify-around">

                                {{-- show details --}}
                                {{-- delete --}}
                                <button
                                    wire:click="removeItem({{ $product->id }})"
                                    class="p-1 text-red-600 hover:bg-red-600 hover:text-white rounded mx-0">
                                    <svg
                                        class="w-5 h-5"
                                        fill="currentColor"
                                        viewBox="0 0 20 20"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <path
                                            fill-rule="evenodd"
                                            d="M9 2a1 1 0 00-.894.553L7.382 4H4a1 1 0 000 2v10a2 2 0 002 2h8a2 2 0 002-2V6a1 1 0 100-2h-3.382l-.724-1.447A1 1 0 0011 2H9zM7 8a1 1 0 012 0v6a1 1 0 11-2 0V8zm5-1a1 1 0 00-1 1v6a1 1 0 102 0V8a1 1 0 00-1-1z"
                                            clip-rule="evenodd"></path>
                                    </svg>
                                </button>
                                {{-- modal delete --}}

                        </td>
                        <td class="px-2 py-2 md:px-5 md:py-5 md:text-lg border-b border-gray-200 bg-white text-md">
                                <div class="flex items-center">

                                    <div class="ml-3">
                                        <p class="text-gray-900 whitespace-no-wrap">
                                            {{ $product->nama_produk }}
                                        </p>
                                    </div>
                                </div>
                        </td>
                        
                        <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                                <p class="text-gray-900 whitespace-no-wrap">
                                    {{ $product->harga }}
                                </p>
                        </td>
                        <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                            <p class="text-gray-900 whitespace-no-wrap">
                                <input wire:model="jum_item" type="number">
                            </p>
                        </td>
                        <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                            <p class="text-gray-900 whitespace-no-wrap">
                                {{ $subtotal }}
                            </p>
                    </td>
                        
                </tbody>
                @empty
                <div class="w-full flex mx-auto font-semibold md:text-3xl text-lg">
                    <dir class="mx-auto">
                        anda belum memiliki Item
                    </dir>
                </div>
            @endforelse
            </table>

            

                    
                    
            
    </div>
    <div class="card-footer">
        @if($cart['product'])
        <button wire:click="checkout()" class="btn btn-success float-right">Checkout</button>
    @endif
    </div>
</div>
