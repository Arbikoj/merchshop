<div>
    {{-- Because she competes with no one, no one can compete with her. --}}
    <div class="fixed z-10 inset-0 overflow-y-auto ease-out duration-400">
        <div
            class="flex items-end justify-center px-3  h-screen pb-10 text-center block ">
            <div class="fixed inset-0 transition-opacity">
                <div class="absolute inset-0 bg-gray-900"></div>
            </div>
            <!-- This element is to trick the browser into centering the modal contents. -->
            <span class="hidden sm:inline-block sm:align-middle sm:h-screen"></span>​

            <div
                class="md:w-2/5 inline-block w-full mx-auto md:flex-wrap mx-auto bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 align-center"
                aria-modal="true"
                aria-labelledby="modal-headline">

                <form>
                    <div class="overflow-auto  w-ful bg-white px-4 pt-2 pb-4">
                        <div class="mb-4 mt-0">
                            {{-- main --}}
                                {{-- konten filter --}}
                                    <div class="grid bg-white rounded-lg shadow-xl w-full">
                                        <div>
                                            <button
                                                wire:click="closeModal()"
                                                class="btn btn-square btn-ghost flex flex-row-reverse"
                                                type="button">
                                                <svg
                                                    xmlns="http://www.w3.org/2000/svg"
                                                    fill="none"
                                                    viewBox="0 0 24 24"
                                                    class="inline-block w-6 h-6 stroke-current text-error">
                                                    <path
                                                        stroke-linecap="round"
                                                        stroke-linejoin="round"
                                                        stroke-width="2"
                                                        d="M6 18L18 6M6 6l12 12"></path>
                                                </svg>

                                            </button>
                                        </div>
                                        <div class="flex mx-auto font-bold justify-center right-0">

                                            <div class="">Add Product</div>

                                        </div>

                                        <a
                                            href="#"
                                            class="text-gray-700 block px-4 py-2 text-sm"
                                            tabindex="-1"
                                            id="content-1">
                                            
                                            <div>Kategori</div>
                                            <select name="" id="">
                                                @foreach ($kategori as $item)
                                                <option>{{ $item->category }}</option>
                                                @endforeach                                                
                                            </select>
                                        </a>                                                                              
                                    </div>

                        </div>
                    </form>
                    
                </div>
            </div>
        </div>
    </div>

</div>