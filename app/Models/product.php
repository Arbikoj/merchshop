<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class product extends Model
{
    use HasFactory;
    protected $table = "products";

    protected $fillable = [
        'id',
        // 'subcategories_id',
        'categories_id',
        'img_products_id',
        'nama_produk',
        'slug',
        'desc_produk',
        'kondisi',
        'harga',
        'diskon',
        'stok',
        'sku',
        'pre_order',
        'release_product',
        'photos'
    ];

    public function kategori()
    {
        return $this->belongsTo(category::class, 'categories_id');
    }

    //slug
    protected static function boot()
    {
        parent::boot();
        static::created(function ($kategori) {
            $kategori->slug = $kategori->createSlug($kategori->nama_produk);
            $kategori->save();
        });
    }

    private function createSlug($category)
    {
        if (static::whereSlug($slug = Str::slug($category))->exists()) {

            $max = static::whereNamaProduk($category)->latest('id')->skip(1)->value('slug');

            if (isset($max[-1]) && is_numeric($max[-1])) {

                return preg_replace_callback('/(\d+)$/', function ($mathces) {

                    return $mathces[1] + 1;
                }, $max);
            }
            return "{$slug}-2";
        }
        return $slug;
    }
    //endslug
    
}
