<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class productVariation extends Model
{
    use HasFactory;

    protected $table = "product_variations";

    protected $fillable = [
            'id',
            'products_id',
            'variation', 
    ];
}
