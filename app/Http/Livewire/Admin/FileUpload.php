<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use App\Models\imgProducts;
use Livewire\WithFileUploads;

class FileUpload extends Component
{
    use WithFileUploads;
    public $gambar;
    public function submit()
    {
        // $validatedData = $this->validate([
        //     'gambar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        // ]);

        // $validatedData['name'] = $this->gambar->store('public/gambar');

        // imgProducts::create($validatedData);
        $this->validate([
                'gambar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
        $this->gambar->store('public/gambar');

        session()->flash('message', 'Image successfully Uploaded.');
    }

    public function render()
    {
        return view('livewire.admin.file-upload');
    }
}
