<?php

namespace App\Http\Livewire\User;

use Livewire\Component;
use App\Models\User;
use App\Models\Address;
use Illuminate\Support\Facades\Auth;



class Alamat extends Component
{
    public $alamat;
    public $id_akun;
    public function render()
    {
        $this->id_akun = Auth::id(); 
        $this->alamat = Address::where('users_id', $this->id_akun)->orderBy('id', 'ASC')->get();
        return view('livewire.user.alamat');
    }

    public function edit($id)
    {
        return redirect()->route('user.addalamat');
    }
}
