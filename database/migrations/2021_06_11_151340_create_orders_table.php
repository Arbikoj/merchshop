<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('detail_users_id')->constrained();
            $table->string('kode_order', 250);
            $table->string('status', 250);
            $table->dateTime('order_date', $precision = 0);
            $table->string('status_pembayaran', 250);
            $table->string('token_pembayaran', 250);
            $table->string('url_pembayaran', 250);
            $table->decimal('harga_dasar');
            $table->decimal('ongkir');
            $table->decimal('subtotal_ongkir');
            $table->decimal('subtotal_produk');
            $table->decimal('total_harga');
            $table->string('opsi_pengiriman', 250);
            $table->string('catatan', 250);
            $table->string('approved_by', 250);
            $table->dateTime('approved_at', $precision = 0);
            $table->string('cancel_by', 250);
            $table->dateTime('canceled_date', $precision = 0);
            $table->string('catatan_pembatalan', 250);
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
