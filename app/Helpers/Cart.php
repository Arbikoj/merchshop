<?php

namespace App\Helpers;

use App\Models\product;

class Cart
{
    public function __construct()
    {
        if ($this->get() === null){
            $this->set($this->empty());
        }
    }

    public function add(product $product)
    {
        $cart = $this->get();

        array_push($cart['product'], $product);

        $this->set($cart);
    }

    public function remove(int $productId)
    {
        $cart = $this->get();
        // dd(array_column($cart['product'], 'id'));
        array_splice($cart['product'], array_search($productId, array_column($cart['product'], 'id')));

        $this->set($cart);
    }
    
    public function clear()
    {
        $this->set($this->empty());
    }

    public function empty ()
    {
        return [
            'product' => [],
        ];
    }

    public function get ()
    {
        return session()->get('cart');
    }

    public function set($cart)
    {
        session()->put('cart', $cart);
    }

}