<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('categories_id')->constrained();
            $table->string('nama_produk', 250);
            $table->string('slug', 250);
            $table->string('desc_produk', 500);
            $table->string('kondisi',10);
            $table->integer('harga');
            $table->decimal('diskon')->default(0);
            $table->integer('stok');
            $table->string('sku',225);
            $table->boolean('pre_order')->default(0);
            $table->boolean('release_product')->default(1)->nullable();
            $table->string('photos')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
