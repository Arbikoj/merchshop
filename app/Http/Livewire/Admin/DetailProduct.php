<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use App\Models\product;
use App\Facades\Cart;


class DetailProduct extends Component
{
    public $detail;
    public function mount($slug){
        // $this->detail = product::where('slug', $slug)->first();
        
        $this->detail = product::where('products.slug', $slug)
        ->join('categories', 'products.categories_id', '=', 'categories.id')
        // ->join('product_variations', 'products.id', '=', 'product_variations.products_id')
        ->select('products.*', 'categories.category', 'categories.desc_category')->orderBy('created_at', 'DESC')->get()->first();
    }

    public function BeliSekarang($id)
    {
        Cart::add(Product::where('id', $id)->first());

        $this->emit('cartAdded');
    }  

    public function render()
    {
        // dd(Cart::get());
        return view('livewire.admin.detail-product');

        // return view('livewire.detail-product',[
        //     'product' => $products, 
        // ]);
    }

}
