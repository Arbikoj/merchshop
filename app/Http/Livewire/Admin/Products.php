<?php

namespace App\Http\Livewire\Admin;

use Carbon\Carbon;
use Livewire\WithPagination;
use Livewire\Component;
use App\Models\product;
use App\Models\category;
use App\Models\subcategory;

use Livewire\WithFileUploads;

class Products extends Component
{
    use WithFileUploads;

    public $edit;
    public $custom_diskon = 0;

    public $produk;
    public $produk_id;
    public // $subcategories_id,
        $categories_id,
        // $img_products_id,
        $nama_produk,
        $slug,
        $desc_produk,
        $kondisi,
        $harga,
        $diskon,
        $stok,
        $sku,
        $pre_order,
        $release_product,
        $photos;

        //variable search
        public $srcItem;
        protected $queryString = ['srcItem'];
        public $isModal = 0;

        // pagination
        public $paginate =10;
        use WithPagination;

        // status value 
        public $statusvalue = 0;

        // kategori
        public $dropdownkategori;
        public $temp_poto;
    public function render()
    {
        
        $this->dropdownkategori = category::all();
        $srcItem = '%'.$this->srcItem.'%';
        return view('livewire.admin.products',[
            'katalog' => product::Where('nama_produk','LIKE',$srcItem)
            ->orWhere('desc_produk','LIKE',$srcItem)
            ->orderBy('created_at', 'DESC')->paginate($this->paginate),
        ]);
    }

    //FUNGSI INI AKAN DIPANGGIL KETIKA TOMBOL TAMBAH ANGGOTA DITEKAN
    public function create()
    {
        //KEMUDIAN DI DALAMNYA KITA MENJALANKAN FUNGSI UNTUK MENGOSONGKAN FIELD
        $this->resetFields();
        //DAN MEMBUKA MODAL
        $this->openModal();
    }

    //FUNGSI INI UNTUK MENUTUP MODAL DIMANA VARIABLE ISMODAL KITA SET JADI FALSE
    public function closeModal()
    {
        $this->isModal = false;
        $this->resetFields();
    }

    //FUNGSI INI DIGUNAKAN UNTUK MEMBUKA MODAL
    public function openModal()
    {
        $this->isModal = true;
    }

    //FUNGSI INI UNTUK ME-RESET FIELD/KOLOM, SESUAIKAN FIELD APA SAJA YANG KAMU MILIKI
    public function resetFields()
    {
    
        $this->produk_id = '';
        $this->categories_id = '';
        $this->photos = '';
        $this->nama_produk = '';
        $this->slug = '';
        $this->desc_produk = '';
        $this->kondisi = '';
        $this->harga = '';
        $this->diskon = '';
        $this->stok = '';
        $this->sku = '';
        $this->pre_order = '';
        $this->release_product = '';
    }

    //METHOD STORE AKAN MENG-HANDLE FUNGSI UNTUK MENYIMPAN / UPDATE DATA
    public function upload()
    {
        $edit = $this->produk_id ? true : false;
        
        //MEMBUAT VALIDASI

        if ($edit) {
            $rules = [
                'nama_produk' => 'required|string',
                'categories_id' => 'required',
                'nama_produk' => 'required',
                'desc_produk' => 'required',
                'kondisi' => 'required',
                'harga' => 'required',
                'diskon'  => 'required',
                'stok'  => 'required',
                'sku' => 'required',
                'pre_order' => 'required',
                'release_product' => 'required',
            ];
        }
        
        if (!$edit) {
            $rules = [
                'nama_produk' => 'required|string',
                'categories_id' => 'required',
                'nama_produk' => 'required',
                'desc_produk' => 'required',
                'kondisi' => 'required',
                'harga' => 'required',
                'diskon'  => 'required',
                'stok'  => 'required',
                'sku' => 'required',
                'pre_order' => 'required',
                'release_product' => 'required',
                'photos' => 'required',
            ];
        }

        $this->validate($rules);

        if ($this->photos && !$edit) {
            $this->photos->store('images','public');
        }

        if ($this->produk_id && $edit) {

            if (strrchr( $this->photos, '.') == '.tmp') {
                // unlink( $this->photos);
                product::updateOrCreate(['id' => $this->produk_id], [
            
                    'categories_id' => $this->categories_id,
                    'photos' => $this->photos->store('images','public'),
                    'nama_produk' => $this->nama_produk,
                    'slug' => $this->slug,
                    'desc_produk' => $this->desc_produk,
                    'kondisi' => $this->kondisi,
                    'harga' => $this->harga,
                    'diskon'  => $this->diskon,
                    'stok'  => $this->stok,
                    'sku' => $this->sku,
                    'pre_order' => $this->pre_order,
                    'release_product' => $this->release_product,
                ]);
            } else {
                product::updateOrCreate(['id' => $this->produk_id], [
            
                    'categories_id' => $this->categories_id,
                    'photos' => $this->photos,
                    'nama_produk' => $this->nama_produk,
                    'slug' => $this->slug,
                    'desc_produk' => $this->desc_produk,
                    'kondisi' => $this->kondisi,
                    'harga' => $this->harga,
                    'diskon'  => $this->diskon,
                    'stok'  => $this->stok,
                    'sku' => $this->sku,
                    'pre_order' => $this->pre_order,
                    'release_product' => $this->release_product,
                ]);
            }
            
                        
        }elseif (!$this->produk_id) {
            product::updateOrCreate([
                'categories_id' => $this->categories_id,
                'photos' => $this->photos->store('images','public'),
                'nama_produk' => $this->nama_produk,
                'slug' => $this->slug,
                'desc_produk' => $this->desc_produk,
                'kondisi' => $this->kondisi,
                'harga' => $this->harga,
                'diskon'  => $this->diskon,
                'stok'  => $this->stok,
                'sku' => $this->sku,
                'pre_order' => $this->pre_order,
                'release_product' => $this->release_product,
            ]);
        } 
        

        //BUAT FLASH SESSION UNTUK MENAMPILKAN ALERT NOTIFIKASI
        session()->flash('message', $this->produk_id ? $this->nama_produk . ' Diperbaharui': $this->nama_produk . ' Ditambahkan');
        $this->closeModal(); //TUTUP MODAL
        $this->resetFields(); //DAN BERSIHKAN FIELD
        // return redirect('/admin/products');
    }

    //FUNGSI INI UNTUK MENGAMBIL DATA DARI DATABASE BERDASARKAN ID MEMBER
    public function edit($id)
    {
        $produk = product::find($id); //BUAT QUERY UTK PENGAMBILAN DATA
        //LALU ASSIGN KE DALAM MASING-MASING PROPERTI DATANYA
        $this->produk_id = $id;

        // $this->subcategories_id = $produk->subcategories_id;
        $this->categories_id = $produk->categories_id;
        $this->photos = $produk->photos;
        $this->nama_produk = $produk->nama_produk;
        $this->slug = $produk->slug;
        $this->desc_produk = $produk->desc_produk;
        $this->kondisi = $produk->kondisi;
        $this->harga = $produk->harga;
        $this->diskon = $produk->diskon;
        $this->stok = $produk->stok;
        $this->sku = $produk->sku;
        $this->pre_order = $produk->pre_order;
        $this->release_product = $produk->release_product;
        

        // return redirect()->route('update');
        $this->openModal();
    }

    public function show($id)
    {
        $produk = product::find($id); //BUAT QUERY UTK PENGAMBILAN DATA
        //LALU ASSIGN KE DALAM MASING-MASING PROPERTI DATANYA
        $this->produk_id = $id;

        // $this->subcategories_id = $produk->subcategories_id;
        $this->categories_id = $produk->categories_id;
        $this->photos = $produk->photos;
        $this->nama_produk = $produk->nama_produk;
        $this->slug = $produk->slug;
        $this->desc_produk = $produk->desc_produk;
        $this->kondisi = $produk->kondisi;
        $this->harga = $produk->harga;
        $this->diskon = $produk->diskon;
        $this->stok = $produk->stok;
        $this->sku = $produk->sku;
        $this->pre_order = $produk->pre_order;
        $this->release_product = $produk->release_product;

        // return redirect()->route('show');
        // $this->openModal();
    }


    //FUNGSI INI UNTUK MENGHAPUS DATA
    public function delete($id)
    {
        $produk = product::find($id); //BUAT QUERY UNTUK MENGAMBIL DATA BERDASARKAN ID
        $produk->delete(); //LALU HAPUS DATA
        session()->flash('message', $produk->nama_produk . ' Dihapus'); //DAN BUAT FLASH MESSAGE UNTUK NOTIFIKASI
        // return redirect()->route('generus');
    }

    public function statusrelease($id)
    {
        $hem = product::find($id);
        
    }

    public function diskon()
    {
        $this->custom_diskon = true;
    }
}
