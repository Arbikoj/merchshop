const defaultTheme = require('tailwindcss/defaultTheme');
const colors = require('tailwindcss/colors')

module.exports = {
    purge: [
        './vendor/laravel/framework/src/Illuminate/Pagination/resources/views/*.blade.p' +
        'hp',
        './vendor/laravel/jetstream/**/*.blade.php',
        './storage/framework/views/*.php',
        './resources/views/**/*.blade.php'
    ],

    theme: {
        colors: {
            // Build your palette here
            transparent: 'transparent',
            current: 'currentColor',
            gray: colors.trueGray,
            red: colors.red,
            teal: colors.teal,
            blue: colors.lightBlue,
            yellow: colors.amber,
            rose: colors.rose,
            pink: colors.pink,
            fuchsia: colors.fuchsia,
            purple: colors.purple,
            indigo: colors.indigo,
            violet: colors.violet,
            lightBlue: colors.lightBlue,
            cyan: colors.cyan,
            emerald: colors.emerald,
            lime: colors.lime,
            orange: colors.orange,
            blueGray: colors.blueGray,
            coolGray: colors.coolGray,
            trueGray: colors.trueGray,

            custom: colors.custom,

        },
        extend: {
            fontFamily: {
                sans: [
                    'Nunito', ...defaultTheme.fontFamily.sans
                ]
            }
        }
    },

    variants: {
        extend: {
            opacity: ['disabled']
        }
    },

    plugins: [
        require('@tailwindcss/forms'), require('@tailwindcss/typography')
    ],

    plugins: [require('daisyui')],
};