<div class="fixed z-10 inset-0 overflow-y-auto ease-out duration-400">
    <div class="flex items-end justify-center min-h-screen px-3 pt-0 pb-20 text-center block ">
        <div class="fixed inset-0 transition-opacity">
            <div class="absolute inset-0 bg-gray-900"></div>
        </div>
        <!-- This element is to trick the browser into centering the modal contents. -->
        <span class="hidden sm:inline-block sm:align-middle sm:h-screen"></span>​

        <div class="inline-block h-full w-full mx-auto md:flex-wrap mx-auto bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 align-center"
            role="dialog"
            aria-modal="true"
            aria-labelledby="modal-headline">

            <form>
              <div class="overflow-auto h-full bg-white px-4 pt-2 pb-4">
                  <div class="mb-4 mt-0">   
                      {{-- main --}}
                      <div class="flex items-center justify-center my-5">
                          <div class="grid bg-white rounded-lg shadow-xl w-full">
                            <div>
                              <button
                                wire:click="closeModal()"
                                    class="btn btn-square btn-ghost flex flex-row-reverse"
                                    type="button">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    fill="none"
                                    viewBox="0 0 24 24"
                                    class="inline-block w-6 h-6 stroke-current text-error">
                                    <path
                                        stroke-linecap="round"
                                        stroke-linejoin="round"
                                        stroke-width="2"
                                        d="M6 18L18 6M6 6l12 12"></path>
                                </svg>
                                
                            </button>
                            </div>
                              <div class="flex mx-auto font-bold justify-center right-0">
                                  
                                  <div class="">Add Product</div>
          
                              </div>
                            {{-- mama produk --}}
                            <div class="grid grid-cols-1 mt-5 mx-7">
                              <label class="uppercase md:text-sm text-xs text-gray-500 text-light font-semibold">Nama Produk</label>
                              <input wire:model="nama_produk" class="py-2 px-3 rounded-lg border-2 border-blue-300 mt-1 focus:outline-none focus:ring-2 focus:ring-blue-600 focus:border-transparent" type="text" placeholder="Nama Produk" />
                              @error('nama_produk') <span class="error text-red-500">{{ $message }}</span> @enderror
                            </div>
                            {{-- kategori --}}
                            <div class="grid grid-cols-1 md:grid-cols-2 gap-5 md:gap-8 mt-5 mx-7">
                              <div class="grid grid-cols-1">
                                <label class="uppercase md:text-sm text-xs text-gray-500 text-light font-semibold">Kategori</label>
                                <select wire:model="categories_id" class="py-2 px-3 rounded-lg border-2 border-blue-300 mt-1 focus:outline-none focus:ring-2 focus:ring-blue-600 focus:border-transparent">
                                  <option>Pilih Kategori</option>
                                  @foreach ($dropdownkategori as $item)
                                      <option value="{{$item->id}}">{{$item->category}}</option>
                                  @endforeach
                                  
                                </select>
                              @error('categories_id') <span class="error text-red-500">{{ $message }}</span> @enderror
                              </div>
                              {{-- harga --}}
                              <div class="grid grid-cols-1">
                                <label class="uppercase md:text-sm text-xs text-gray-500 text-light font-semibold">Harga</label>
                                <input wire:model="harga" class="py-2 px-3 rounded-lg border-2 border-blue-300 mt-1 focus:outline-none focus:ring-2 focus:ring-blue-600 focus:border-transparent" type="text" placeholder="Rp." />
                                @error('harga') <span class="error text-red-500">{{ $message }}</span> @enderror
                              </div>
                            </div>
                            {{-- kondisi --}}
                            <div class="grid grid-cols-1 md:grid-cols-2 gap-5 md:gap-8 mt-5 mx-7">
                              <div class="grid grid-cols-1">
                                <label class="uppercase md:text-sm text-xs text-gray-500 text-light font-semibold">Kondisi</label>
                                <select wire:model="kondisi" class="py-2 px-3 rounded-lg border-2 border-blue-300 mt-1 focus:outline-none focus:ring-2 focus:ring-blue-600 focus:border-transparent">
                                  <option>Kondisi</option>
                                  <option>Baru</option>
                                  <option>Bekas</option>
                                </select>
                              @error('kondisi') <span class="error text-red-500">{{ $message }}</span> @enderror
                              </div>
                              {{-- diskon --}}
                              <div class="grid grid-cols-1">

                                <label class="uppercase md:text-sm text-xs text-gray-500 text-light font-semibold">Diskon</label>
                                <input wire:model="diskon" class="py-2 px-3 rounded-lg border-2 border-blue-300 mt-1 focus:outline-none focus:ring-2 focus:ring-blue-600 focus:border-transparent" type="text" placeholder="Diskon" />
                                
                                @error('diskon') <span class="error text-red-500">{{ $message }}</span> @enderror

                                {{-- <button wire:model="custom_diskon()" value="1">Custom</button> --}}
                              </div>
                            </div>
                            {{-- stok --}}
                            <div class="grid grid-cols-1 md:grid-cols-2 gap-5 md:gap-8 mt-5 mx-7">
                              <div class="grid grid-cols-1">
                                <div class="grid grid-cols-1">
                                  <label class="uppercase md:text-sm text-xs text-gray-500 text-light font-semibold">Stok</label>
                                  <input wire:model="stok" class="py-2 px-3 rounded-lg border-2 border-blue-300 mt-1 focus:outline-none focus:ring-2 focus:ring-blue-600 focus:border-transparent" type="text" placeholder="Stok" />
                              @error('stok') <span class="error text-red-500">{{ $message }}</span> @enderror
                                </div>
                              </div>
                              {{-- SKU --}}
                              <div class="grid grid-cols-1">
                                <label class="uppercase md:text-sm text-xs text-gray-500 text-light font-semibold">SKU</label>
                                <input wire:model="sku" class="py-2 px-3 rounded-lg border-2 border-blue-300 mt-1 focus:outline-none focus:ring-2 focus:ring-blue-600 focus:border-transparent" type="text" placeholder="SKU" />
                            @error('sku') <span class="error text-red-500">{{ $message }}</span> @enderror
                              </div>
                            </div>
                            {{-- preorder --}}
                            <div class="grid grid-cols-1 mt-5 mx-7">
                              <label class="uppercase md:text-sm text-xs text-gray-500 text-light font-semibold">Preorder</label>
                              <select wire:model="pre_order" class="py-2 px-3 rounded-lg border-2 border-blue-300 mt-1 focus:outline-none focus:ring-2 focus:ring-blue-600 focus:border-transparent">
                                <option>Pre Order</option>
                                <option value="1">Ya</option>
                                <option value="0">Tidak</option>
                              </select>
                            @error('pre_order') <span class="error text-red-500">{{ $message }}</span> @enderror
                            </div>
                            {{-- tampilkan product --}}
                            <div class="grid grid-cols-1 mt-5 mx-7">
                              <label class="uppercase md:text-sm text-xs text-gray-500 text-light font-semibold">tampilkan produk</label>
                              <select wire:model="release_product" class="py-2 px-3 rounded-lg border-2 border-blue-300 mt-1 focus:outline-none focus:ring-2 focus:ring-blue-600 focus:border-transparent">
                                <option>Tampilkan Produk</option>
                                <option value="1">Ya</option>
                                <option value="0">Tidak</option>
                              </select>
                            @error('release_product') <span class=" text-red-500 error">{{ $message }}</span> @enderror
                            </div>
                            {{-- deskripsi --}}
                            <div class="grid grid-cols-1 mt-5 mx-7">
                              <label class="uppercase md:text-sm text-xs text-gray-500 text-light font-semibold">Deskripsi</label>
                              <textarea id="t" onkeydown="if(event.keyCode == 13) return false;" onkeypress="onTestChange();" wire:model="desc_produk" rows="10" class="py-2 px-3 rounded-lg border-2 border-blue-300 mt-1 focus:outline-none focus:ring-2 focus:ring-blue-600 focus:border-transparent resize-y" type="text" placeholder="Deskripsi" />
                              @error('desc_produk') <span class="error text-red-500">{{ $message }}</span> @enderror
                            </div>
                            <textarea name="" id="" cols="30" rows="10"></textarea>
                            {{-- upload foto --}}
                            <div class="grid grid-cols-1 mt-5 mx-7">
                              <label class="uppercase md:text-sm text-xs text-gray-500 text-light font-semibold mb-1">Upload Photo</label>
                                <div class='flex w-full'>
                                        <input type="file" wire:model="photos">
                                        @if ($photos && !$this->produk_id)
                                            Preview foto :
                                            <img src="{{ $photos->temporaryUrl() }}" style="width: 120px; height:auto;">
                                        {{-- @elseif($id = $this->produk_id)) --}}
                                        @elseif ($photos && $this->produk_id)
                                            {{$this->produk_id}}
                                            <img src="{{asset('storage/'.$this->photos)}}" style="width: 120px; height:auto;">
                                        @elseif(strrchr($this->photos, '.') == '.tmp')
                                            <img src="{{ $photos->temporaryUrl() }}" style="width: 120px; height:auto;">
                                        @endif
                                        {{-- @if ($id = $this->produk_id && !$photos)
                                          <img src="{{asset('storage/'.$this->photos)}}" style="width: 120px; height:auto;">
                                        @endif --}}
                                </div>
                            @error('photos') <span class=" text-red-500 error">{{ $message }}</span> @enderror
                                
                            </div>
                            {{-- tombol --}}
                            <div class='flex items-center justify-center  md:gap-8 gap-4 pt-5 pb-5'>
                              <button wire:click="closeModal()" type="button" class='w-auto bg-gray-500 hover:bg-gray-700 rounded-lg shadow-xl font-medium text-white px-4 py-2'>Batal</button>
                              <button wire:click.prevent="upload()" class='w-auto bg-blue-500 hover:bg-blue-700 rounded-lg shadow-xl font-medium text-white px-4 py-2'>
                                @if ($id = $this->produk_id)
                                  Simpan
                                @else
                                  Buat  
                                @endif
                                
                              </button>
                            </div>
                        
                          </div>
                        </div>
                      
                  </div>
              </div>
          </form>
        </div>
    </div>
</div>
</div>
<script type="text/javascript">
  function formatTextArea(textArea) {
      textArea.value = textArea.value.replace(/(^|\r\n|\n)([^*]|$)/g, "$1*$2");
  }

  window.onload = function() {
      var textArea = document.getElementById("t");
      textArea.onkeyup = function(evt) {
          evt = evt || window.event;

          if (evt.keyCode == 13) {
              formatTextArea(this);
          }
      };
  };
</script>