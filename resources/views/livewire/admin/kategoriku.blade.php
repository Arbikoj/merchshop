
<div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
    <div class="block bg-white overflow-hidden shadow-xl sm:rounded-lg px-4 ">
            @if (session()->has('message'))
                <div
                    class="bg-green-100 border-t-4 border-green-500 rounded-b text-green-900 px-4 py-3 shadow-md my-3"
                    role="alert">
                    <div class="flex">
                        <div>
                            <p class="text-sm">{{ session('message') }}</p>
                        </div>
                    </div>
                </div>
                @endif
                <div class="block">
                    
                </div>
                <div class="md:flex">
                    

                    <label class="mb-1 md:hidden">Nama Kategori</label>
                    <input
                        placeholder="Nama Kategori"
                        wire:model="category"
                        class="appearance-none rounded-r rounded-l sm:rounded-l-none border border-gray-400 border-b block pl-8 pr-6 py-2 w-full bg-white text-sm placeholder-gray-400 text-gray-700 focus:bg-white focus:placeholder-gray-600 focus:text-gray-700 focus:outline-none"/>
                    @error('category') <span class="md:hidden error text-red-500 py-3 px-3 rounded-lg">{{ $message }}</span> @enderror
                        
                        @if ($id = $this->category_id)
                    
                    <label class="mb-1 md:hidden">Slug</label>
                        <input
                            placeholder="Slug"
                            wire:model="slug"
                            class="appearance-none rounded-r rounded-l sm:rounded-l-none border border-gray-400 border-b block pl-8 pr-6 py-2 w-full bg-white text-sm placeholder-gray-400 text-gray-700 focus:bg-white focus:placeholder-gray-600 focus:text-gray-700 focus:outline-none"/>
                        
                    @endif
                    <br>
                    <label class="mb-1 md:hidden">Deskripsi</label>
                        <input
                        placeholder="Deskripsi"
                        wire:model="desc_category"
                        class="appearance-none rounded-r rounded-l sm:rounded-l-none border border-gray-400 border-b block pl-8 pr-6 py-2 w-full bg-white text-sm placeholder-gray-400 text-gray-700 focus:bg-white focus:placeholder-gray-600 focus:text-gray-700 focus:outline-none"/>
                        @error('desc_category') <span class="md:hidden error text-red-500 py-3 px-3 rounded-lg">{{ $message }}</span> @enderror
                    
                </div>
                <div>
                    <button
                        wire:click="store()"
                        class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded my-3">
                        @if ($id = $this->category_id)
                            Save
                        @else
                            Tambah Kategori
                        @endif 
                    </button>
                    @if ($id = $this->category_id)
                        <button
                            wire:click="resetFields()"
                            class="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded my-3">
                            Batal 
                        </button>
                    @endif
                    
                </div>
                            
                <div>
                    <h2 class="text-2xl font-semibold leading-tight">Kategori</h2>
                </div>
                <div class="my-2 flex flex-row">
                    <div class="flex flex-row mb-1 sm:mb-0">
                        <div class="relative">
                            <select
                                wire:model="paginate"
                                class="appearance-none h-full rounded-l border block appearance-none w-full bg-white border-gray-400 text-gray-700 py-2 px-4 pr-8 leading-tight focus:outline-none focus:bg-white focus:border-gray-500">
                                <option value="10">10</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                                <option value="">All</option>
                            </select>
                            <div
                                class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                                {{-- <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                    <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
                                </svg> --}}
                            </div>
                        </div>
                        
                    </div>
                    <div class="block relative">
                        <span class="h-full absolute inset-y-0 left-0 flex items-center pl-2">
                            <svg viewBox="0 0 24 24" class="h-4 w-4 fill-current text-gray-500">
                                <path
                                    d="M10 4a6 6 0 100 12 6 6 0 000-12zm-8 6a8 8 0 1114.32 4.906l5.387 5.387a1 1 0 01-1.414 1.414l-5.387-5.387A8 8 0 012 10z"></path>
                            </svg>
                        </span>
                        <input
                            placeholder="Search"
                            wire:model="srcItem"
                            class="appearance-none rounded-r rounded-l sm:rounded-l-none border border-gray-400 border-b block pl-8 pr-6 py-2 w-full bg-white text-sm placeholder-gray-400 text-gray-700 focus:bg-white focus:placeholder-gray-600 focus:text-gray-700 focus:outline-none"/>
                    </div>
                </div>
                <div class="-mx-4 sm:-mx-8 px-4 sm:px-8 py-4 overflow-x-auto">
                    <div class="inline-block min-w-full shadow rounded-lg overflow-hidden">
                        <table class="min-w-full leading-normal">
                            <thead>
                                <tr>
                                    <th
                                        class="w-5  px-2 py-3 justify-center border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                                        <span class="mx-auto font-bold justify-center">Opsi</span>
                                    </th>
                                    <th
                                        class="mx-auto px-2 py-3 justify-center border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                                        <span class="font-bold justify-center">Nama Kategori</span>
                                    </th>
                                    <th
                                        class="mx-auto px-2 py-3 justify-center border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                                        <span class="font-bold justify-center">Slug</span>
                                    </th>
                                    <th
                                        class="mx-auto px-2 py-3 justify-center border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                                        <span class="font-bold justify-center">Deskripsi</span>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($kategori as $row)
                                <tr>
                                    <td class="px-2 py-2 md:px-5 md:py-5 md:text-lg text-md border-b border-gray-200 bg-white">
                                        <div class="flex justify-around">

                                            
                                            {{-- edit --}}
                                            <button
                                                wire:click="edit({{ $row->id }})"
                                                class="p-1 text-red-600 hover:bg-red-600 hover:text-white rounded mx-0">
                                                <svg
                                                    class="w-5 h-5"
                                                    fill="black"
                                                    viewBox="0 0 20 20"
                                                    xmlns="http://www.w3.org/2000/svg">
                                                    <path
                                                        d="M13.586 3.586a2 2 0 112.828 2.828l-.793.793-2.828-2.828.793-.793zM11.379 5.793L3 14.172V17h2.828l8.38-8.379-2.83-2.828z"></path>
                                                </svg>
                                            </button>
                                            {{-- delete --}}
                                            <button onclick="return confirm('Are you sure?')" wire:click="delete({{$row->id}})" class="confirm-deletebtn btn-ghost btn-sm modal-button p-1 text-red-600 hover:bg-red-600 hover:text-white rounded mx-0">
                                                <svg
                                                    class="w-5 h-5"
                                                    fill="currentColor"
                                                    viewBox="0 0 20 20"
                                                    xmlns="http://www.w3.org/2000/svg">
                                                    <path
                                                        fill-rule="evenodd"
                                                        d="M9 2a1 1 0 00-.894.553L7.382 4H4a1 1 0 000 2v10a2 2 0 002 2h8a2 2 0 002-2V6a1 1 0 100-2h-3.382l-.724-1.447A1 1 0 0011 2H9zM7 8a1 1 0 012 0v6a1 1 0 11-2 0V8zm5-1a1 1 0 00-1 1v6a1 1 0 102 0V8a1 1 0 00-1-1z"
                                                        clip-rule="evenodd"></path>
                                                </svg>    
                                            </button> 
                                            {{-- <input type="checkbox" id="my-modal-2" class="modal-toggle"> 
                                            <div class="modal">
                                            <div class="modal-box">
                                            <p>Apakah Anda ingin menghapus Kategori {{$row->category}}</p> 
                                                <div class="modal-action">
                                                <button wire:click="delete({{$row->{{ $loop->iteration }}}})" class="btn btn-error">Delete</button> 
                                                <label for="my-modal-2" class="btn">Close</label>
                                                </div>
                                            </div>
                                            </div> --}}
                                            

                                            {{-- modal delete --}}

                                        </td>
                                        <td
                                            class="px-2 py-2 md:px-5 md:py-5 md:text-lg border-b border-gray-200 bg-white text-md">
                                            <div class="flex items-center">

                                                <div class="ml-3">
                                                    <p class="text-gray-900 whitespace-no-wrap">
                                                        {{ $row->category }}
                                                    </p>
                                                </div>
                                            </div>
                                        </td>
                                        <td
                                            class="px-2 py-2 md:px-5 md:py-5 md:text-lg border-b border-gray-200 bg-white text-md">
                                            <p class="text-gray-900 whitespace-no-wrap">{{ $row->slug }}</p>
                                        </td>
                                        <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                                            <p class="text-gray-900 whitespace-no-wrap">
                                                {{ $row->desc_category }}
                                            </p>
                                        </td>
                                    </tr>
                                    
                                    @if ($validateDelete != false)
                                    <x-jet-confirmation-modal wire:model="confirmingUserDeletion">
                                        <x-slot name="title">
                                            Delete Account
                                        </x-slot>
                                    
                                        <x-slot name="content">
                                            Are you sure you want to delete your account? Once your account is deleted, all of its resources and data will be permanently deleted.
                                        </x-slot>
                                    
                                        <x-slot name="footer">
                                            <x-jet-secondary-button wire:click="closeDelete()" wire:loading.attr="disabled">
                                                Nevermind
                                            </x-jet-secondary-button>
                                    
                                            <x-jet-danger-button class="ml-2" wire:click="deleteUser" wire:loading.attr="disabled">
                                                Delete
                                            </x-jet-danger-button>
                                        </x-slot>
                                    </x-jet-confirmation-modal>
                                    @endif
                                    

                                    @empty
                                    <tr>
                                        <td class="border px-4 py-2 text-center text-md font-bold" colspan="5">Tidak ada data</td>
                                    </tr>
                                    @endforelse
                                </tbody>

                            </table>

                        </div>
                    </div>
                    <div class="px-10 pt-2">{{ $kategori->links() }}</div>

            </div>
        </div>
    </div>
</div>
