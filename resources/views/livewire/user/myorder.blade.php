<div>
    {{-- Stop trying to control. --}}
    pesanan kuu
    <div class="card-body">
        <ul class="list-group">
            @forelse ($cart['product'] as $product)
                <li class="list-group-item">
                    <span>{{ $product->nama_produk }} | Price : {{ $product->harga }} </span>
                
                <div class="float-right">
                <button wire:click="removeItem({{ $product->id }})" class="btn btn-danger">Remove</button>
                </div>
            </li>

            @empty
                <div class="w-full flex mx-auto font-semibold md:text-3xl text-lg">
                    <dir class="mx-auto">
                        anda belum memiliki Item
                    </dir>
                </div>
            @endforelse
            
        </ul>
</div>
</div>
