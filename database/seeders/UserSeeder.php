<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = User::create([
            'name' => 'Dhimas MerchShop',
            'role' => '1',
            'email' => 'dhimas@admin.com',
            'password' => bcrypt('12345678'),
        ]);
        $admin->assignRole('admin');
        
        $admin = User::create([
            'name' => 'Arbi MerchShop',
            'role' => '1',
            'email' => 'arbi@admin.com',
            'password' => bcrypt('12345678')
        ]);
        $admin->assignRole('admin');

        $admin = User::create([
            'name' => 'Maulina MerchShop',
            'role' => '1',
            'email' => 'maulina@admin.com',
            'password' => bcrypt('12345678')
        ]);
        
        $admin->assignRole('admin');

        $user = User::create([
            'name' => 'User MerchShop',
            'role' => '0',
            'email' => 'user@user.com',
            'password' => bcrypt('12345678')
        ]);

        $user->assignRole('user');
    }
}
