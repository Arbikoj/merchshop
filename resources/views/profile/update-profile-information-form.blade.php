<x-jet-form-section submit="updateProfileInformation">
    {{-- <x-slot name="title">
        {{ __('Profile Information') }}
    </x-slot> --}}

    {{-- <x-slot name="description">
        {{ __('Update your account\'s profile information and email address.') }}
    </x-slot> --}}
    <x-slot name="form">
        <div class="block w-full">
        <div class="flex">
        <!-- Profile Photo -->
        @if (Laravel\Jetstream\Jetstream::managesProfilePhotos())
            <div x-data="{photoName: null, photoPreview: null}" class="col-span-6 sm:col-span-4">
                <!-- Profile Photo File Input -->
                <input type="file" class="hidden"
                            wire:model="photo"
                            x-ref="photo"
                            x-on:change="
                                    photoName = $refs.photo.files[0].name;
                                    const reader = new FileReader();
                                    reader.onload = (e) => {
                                        photoPreview = e.target.result;
                                    };
                                    reader.readAsDataURL($refs.photo.files[0]);
                            " />

                <x-jet-label for="photo" value="{{ __('Photo') }}" />

                <!-- Current Profile Photo -->
                <div class="mt-2 flex mx-auto" x-show="! photoPreview">
                    <img src="{{ $this->user->profile_photo_url }}" alt="{{ $this->user->name }}" class="rounded-full h-20 w-20 object-cover">
                </div>

                <!-- New Profile Photo Preview -->
                <div class="mt-2" x-show="photoPreview">
                    <span class="block rounded-full w-20 h-20"
                        x-bind:style="'background-size: cover; background-repeat: no-repeat; background-position: center center; background-image: url(\'' + photoPreview + '\');'">
                    </span>
                </div>

                <x-jet-secondary-button class="mt-2 mr-2" type="button" x-on:click.prevent="$refs.photo.click()">
                    {{ __('Select A New Photo') }}
                </x-jet-secondary-button>

                @if ($this->user->profile_photo_path)
                    <x-jet-secondary-button type="button" class="mt-2" wire:click="deleteProfilePhoto">
                        {{ __('Remove Photo') }}
                    </x-jet-secondary-button>
                @endif

                <x-jet-input-error for="photo" class="mt-2" />
            </div>
        @endif
        </div>
        <div class="flex grid grid-cols-1 md:grid-cols-2 gap-5 md:gap-8 mt-5 mx-7">
        <!-- Name -->
            <div class="grid grid-cols-1">
                <x-jet-label for="name" value="{{ __('Nama') }}"/>
                <input
                    id="name"
                    type="text"
                    wire:model.defer="state.name"
                    autocomplete="name"
                    class="w-full py-2 px-3 rounded-lg border-2 border-blue-300 mt-1 focus:outline-none focus:ring-2 focus:ring-blue-600 focus:border-transparent"/>
                <x-jet-input-error for="name" class="mt-2"/>
            </div>

            <!-- Email -->
            <div class="grid grid-cols-1">
                <x-jet-label for="no_telepon" value="{{ __('No. Telp') }}"/>
                <x-jet-input
                    id="email"
                    type="text"
                    wire:model.defer="state.no_telepon"
                    class="w-full py-2 px-3 rounded-lg border-2 border-blue-300 mt-1 focus:outline-none focus:ring-2 focus:ring-blue-600 focus:border-transparent"/>
                <x-jet-input-error for="no_telepon" class="mt-2"/>
            </div>
        </div>
        <div class="grid grid-cols-1 md:grid-cols-2 gap-5 md:gap-8 mt-5 mx-7">
            {{-- jenis kelamin --}}
            <div class="grid grid-cols-1">
                <x-jet-label for="jenis_kelamin" value="{{ __('Jenis Kelamin') }}"/>
                <select
                    wire:model.defer="state.jenis_kelamin"
                    class="py-2 px-3 rounded-lg border-2 border-blue-300 mt-1 focus:outline-none focus:ring-2 focus:ring-blue-600 focus:border-transparent">
                    <option>Pilih</option>
                    <option>Laki-Laki</option>
                    <option>Perempuan</option>
                </select>
                <x-jet-input-error for="jenis_kelamin" class="mt-2"/>

            </div>
            {{-- tanggal_lahir --}}
            <div class="grid grid-cols-1">
                <x-jet-label for="jenis_kelamin" value="{{ __('Tanggal Lahir') }}"/>
                    <x-jet-input
                    id="tanggal_lahir"
                    type="date"
                    wire:model.defer="state.tanggal_lahir"
                    class="w-full py-2 px-3 rounded-lg border-2 border-blue-300 mt-1 focus:outline-none focus:ring-2 focus:ring-blue-600 focus:border-transparent"/>
                <x-jet-input-error for="tanggal_lahir" class="mt-2"/>
            </div>
        </div>
        
    </div>

    </x-slot>

    <x-slot name="actions">
        <x-jet-action-message class="mr-3" on="saved">
            {{ __('Tersimpan.') }}
            

        </x-jet-action-message>

        <x-jet-button wire:loading.attr="disabled" wire:target="photo">
            {{ __('Simpan') }}
            
        </x-jet-button>
    </x-slot>
</x-jet-form-section>
