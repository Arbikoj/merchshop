<?php

namespace App\Http\Livewire\Admin;
use App\Models\subcategory;
use Livewire\Component;

class Subkategori extends Component
{
    public $subkategori;
    public function render()
    {
        $this->subkategori = subcategory::all();
        return view('livewire.admin.subkategori');
    }
}
