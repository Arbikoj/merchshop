<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class subcategory extends Model
{
    use HasFactory;

    protected $table = "subcategories";

    protected $fillable = [
            'id',
            'categories_id',
            'parent_id',
            'subcategory',
            'slug'  
    ];

    // public function product()
    // {
    //     return $this->hasMany(product::class);
    // }

    public function category()
    {
        return $this->belongsTo(category::class);
    }
}
