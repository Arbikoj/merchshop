<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class alamat_provinsi extends Model
{
    use HasFactory;
    protected $table = "alamat_provinsi";

    protected $fillable = [
        'id',
        'alamats_id',
        'nama_provinsi'
    ];

}
