<div>
    <div
        class=" flex flex-col flex-auto flex-shrink-0 text-black">


        <!-- Sidebar -->
        <div class="fixed flex flex-col top-10 left-0 w-14 hover:w-64 md:w-64 bg-gray-300 h-full text-black border-none z-10">
            <div
                class="overflow-y-auto overflow-x-hidden flex flex-col justify-between flex-grow">
                <ul class="flex flex-col py-4 space-y-1">
                    
                        @if (Laravel\Jetstream\Jetstream::managesProfilePhotos())
                        <div
                            class="hidden md:flex mx-auto text-sm border-2 border-transparent rounded-full focus:outline-none focus:border-gray-300 transition">
                            <div class="flex mx-auto">
                                <img
                                class="h-20 w-20 rounded-full object-cover"
                                src="{{ Auth::user()->profile_photo_url }}"
                                alt="{{ Auth::user()->name }}"/>
                            </div>
                            
                        </div>
                        @endif
                        <div class="hidden md:flex mx-auto">{{ Auth::user()->name }}</div>
                    
                    
                    <li class="px-5 hidden md:block">
                        <div class="flex flex-row items-center h-8">
                            <div class="text-sm font-light tracking-wide text-gray-500 uppercase">Main</div>
                        </div>
                    </li>
                    <li>
                        <a
                            href="{{route('user.dashboard')}}"
                            class="relative flex flex-row items-center h-11 focus:outline-none  text-white-600 hover:text-white-800 border-l-4 border-transparent hover:border-blue-500 pr-6">
                            <span class="inline-flex justify-center items-center ml-4">
                                <svg
                                    class="w-5 h-5"
                                    fill="none"
                                    stroke="currentColor"
                                    viewBox="0 0 24 24"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        stroke-linecap="round"
                                        stroke-linejoin="round"
                                        stroke-width="2"
                                        d="M3 12l2-2m0 0l7-7 7 7M5 10v10a1 1 0 001 1h3m10-11l2 2m-2-2v10a1 1 0 01-1 1h-3m-6 0a1 1 0 001-1v-4a1 1 0 011-1h2a1 1 0 011 1v4a1 1 0 001 1m-6 0h6"></path>
                                </svg>
                            </span>
                            <span class="ml-2 text-sm tracking-wide truncate">Dashboard</span>
                        </a>
                    </li>
                    <li>
                        <a
                            href="{{route('user.cart')}}"
                            class="relative flex flex-row items-center h-11 focus:outline-none hover:bg-blue-800 text-white-600 hover:text-white-800 border-l-4 border-transparent hover:border-blue-500 pr-6">
                            <span class="inline-flex justify-center items-center ml-4">
                                <svg
                                    class="w-5 h-5"
                                    fill="currentColor"
                                    viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M 3 1 a 1 1 0 0 0 0 2 h 1.22 l 0.305 1.222 a 0.997 0.997 0 0 0 0.01 0.042 l 1.358 5.43 l -0.893 0.892 C 3.74 11.846 4.632 14 6.414 14 H 15 a 1 1 0 0 0 0 -2 H 6.414 l 1 -1 H 14 a 1 1 0 0 0 0.894 -0.553 l 3 -6 A 1 1 0 0 0 17 3 H 6.28 l -0.31 -1.243 A 1 1 0 0 0 5 1 H 3 z M 16 16.5 a 1.5 1.5 0 1 1 -3 0 a 1.5 1.5 0 0 1 3 0 z M 6.5 18 a 1.5 1.5 0 1 0 0 -3 a 1.5 1.5 0 0 0 0 3 z M 7 5 L 15 5 L 13 9 L 8 9 L 7 5"></path>
                                </svg>
                            </span>
                            <span class="ml-2 text-sm tracking-wide truncate">Keranjang Saya</span>
                            {{-- <span
                                class="hidden md:block px-2 py-0.5 ml-auto text-xs font-medium tracking-wide text-blue-500 bg-indigo-50 rounded-full">New</span> --}}
                        </a>
                    </li>
                    <li>
                        <a
                            href="{{route('user.myorder')}}"
                            class="relative flex flex-row items-center h-11 focus:outline-none hover:bg-blue-800 text-white-600 hover:text-white-800 border-l-4 border-transparent hover:border-blue-500 pr-6">
                            <span class="inline-flex justify-center items-center ml-4">
                                <svg class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M10 2a4 4 0 00-4 4v1H5a1 1 0 00-.994.89l-1 9A1 1 0 004 18h12a1 1 0 00.994-1.11l-1-9A1 1 0 0015 7h-1V6a4 4 0 00-4-4zm2 5V6a2 2 0 10-4 0v1h4zm-6 3a1 1 0 112 0 1 1 0 01-2 0zm7-1a1 1 0 100 2 1 1 0 000-2z" clip-rule="evenodd"></path></svg>
                            </span>
                            <span class="ml-2 text-sm tracking-wide truncate">Pesanan Saya</span>
                            {{-- <span
                                class="hidden md:block px-2 py-0.5 ml-auto text-xs font-medium tracking-wide text-blue-500 bg-indigo-50 rounded-full">New</span> --}}
                        </a>
                    </li>
                    <li>
                        <a
                            href="{{route('user.alamat')}}"
                            class="relative flex flex-row items-center h-11 focus:outline-none hover:bg-blue-800 text-white-600 hover:text-white-800 border-l-4 border-transparent hover:border-blue-500 pr-6">
                            <span class="inline-flex justify-center items-center ml-4">
                                <svg class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M 5.05 4.05 a 7 7 0 1 1 9.9 9.9 L 10 18.9 l -4.95 -4.95 a 7 7 0 0 1 0 -9.9 z M 10 16 C 18 10 15 4 10 4 C 5 4 2 10 10 16 z M 10 7 C 12.568 7.04 12.707 10.788 10 11 C 7.264 10.733 7.431 7.04 10 7" clip-rule="evenodd"></path></svg>
                            </span>
                            <span class="ml-2 text-sm tracking-wide truncate">Alamat Saya</span>
                        </a>
                    </li>
                    {{-- <li>
                        <a
                            href="#"
                            class="relative flex flex-row items-center h-11 focus:outline-none hover:bg-blue-800 text-white-600 hover:text-white-800 border-l-4 border-transparent hover:border-blue-500  pr-6">
                            <span class="inline-flex justify-center items-center ml-4">
                                <svg
                                    class="w-5 h-5"
                                    fill="none"
                                    stroke="currentColor"
                                    viewBox="0 0 24 24"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        stroke-linecap="round"
                                        stroke-linejoin="round"
                                        stroke-width="2"
                                        d="M15 17h5l-1.405-1.405A2.032 2.032 0 0118 14.158V11a6.002 6.002 0 00-4-5.659V5a2 2 0 10-4 0v.341C7.67 6.165 6 8.388 6 11v3.159c0 .538-.214 1.055-.595 1.436L4 17h5m6 0v1a3 3 0 11-6 0v-1m6 0H9"></path>
                                </svg>
                            </span>
                            <span class="ml-2 text-sm tracking-wide truncate">Notifications</span>
                            <span
                                class="hidden md:block px-2 py-0.5 ml-auto text-xs font-medium tracking-wide text-red-500 bg-red-50 rounded-full">1.2k</span>
                        </a>
                    </li> --}}
                    <li class="px-5 hidden md:block">
                        <div class="flex flex-row items-center mt-5 h-8">
                            <div class="text-sm font-light tracking-wide text-gray-500 uppercase">Settings</div>
                        </div>
                    </li>
                    <li>
                        <a
                            href="{{route('user.akun')}}"
                            class="relative flex flex-row items-center h-11 focus:outline-none hover:bg-blue-800 text-white-600 hover:text-white-800 border-l-4 border-transparent hover:border-blue-500 pr-6">
                            <span class="inline-flex justify-center items-center ml-4">
                                <svg
                                    class="w-5 h-5"
                                    fill="none"
                                    stroke="currentColor"
                                    viewBox="0 0 24 24"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        stroke-linecap="round"
                                        stroke-linejoin="round"
                                        stroke-width="2"
                                        d="M16 7a4 4 0 11-8 0 4 4 0 018 0zM12 14a7 7 0 00-7 7h14a7 7 0 00-7-7z"></path>
                                </svg>
                            </span>
                            <span class="ml-2 text-sm tracking-wide truncate">Informasi Akun</span>
                        </a>
                    </li>
                    <li>
                        <a
                            href="{{route('user.setting')}}"
                            class="relative flex flex-row items-center h-11 focus:outline-none hover:bg-blue-800 text-white-600 hover:text-white-800 border-l-4 border-transparent hover:border-blue-500 pr-6">
                            <span class="inline-flex justify-center items-center ml-4">
                                <svg
                                    class="w-5 h-5"
                                    fill="none"
                                    stroke="currentColor"
                                    viewBox="0 0 24 24"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        stroke-linecap="round"
                                        stroke-linejoin="round"
                                        stroke-width="2"
                                        d="M10.325 4.317c.426-1.756 2.924-1.756 3.35 0a1.724 1.724 0 002.573 1.066c1.543-.94 3.31.826 2.37 2.37a1.724 1.724 0 001.065 2.572c1.756.426 1.756 2.924 0 3.35a1.724 1.724 0 00-1.066 2.573c.94 1.543-.826 3.31-2.37 2.37a1.724 1.724 0 00-2.572 1.065c-.426 1.756-2.924 1.756-3.35 0a1.724 1.724 0 00-2.573-1.066c-1.543.94-3.31-.826-2.37-2.37a1.724 1.724 0 00-1.065-2.572c-1.756-.426-1.756-2.924 0-3.35a1.724 1.724 0 001.066-2.573c-.94-1.543.826-3.31 2.37-2.37.996.608 2.296.07 2.572-1.065z"></path>
                                    <path
                                        stroke-linecap="round"
                                        stroke-linejoin="round"
                                        stroke-width="2"
                                        d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"></path>
                                </svg>
                            </span>
                            <span class="ml-2 text-sm tracking-wide truncate">Settings</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- ./Sidebar -->

        {{-- konten --}}
    </div>
</div>
