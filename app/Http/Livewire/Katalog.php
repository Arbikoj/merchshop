<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\product;
use App\Models\category;
use App\Facades\Cart;
use Livewire\WithPagination;



class Katalog extends Component
{
    public $pusing;
    public $isModal = 0;
    public $paginate = 10;

    public $search;
    public $kategori;

    public $srckategori;
    protected $queryString = ['search'];
    public function render()
    {


        $this->kategori = category::all();
        if ($this->srckategori) {
            return view('livewire.katalog',[
            'katalog' => product::where('release_product','1')
            ->where('categories_id', $this->srckategori)
            ->where('nama_produk', 'like', '%'.$this->search.'%')
            ->paginate($this->paginate)
            ,
        ]);
        }else {
            return view('livewire.katalog',[
            'katalog' => product::where('release_product','1')
                ->where('nama_produk', 'like', '%'.$this->search.'%')
                ->paginate($this->paginate),
            ]);
        }
        

        
    }

    public function filter()
    {
        $this->openModal();
    }

    public function closeModal()
    {
        $this->isModal = false;
    }

    public function openModal()
    {
        $this->isModal = true;
    }
}
