<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ mix('css/app.css') }}">

        @livewireStyles
        {{-- daisyui --}}
        <link href="https://cdn.jsdelivr.net/npm/daisyui@0.20.0/dist/full.css" rel="stylesheet" type="text/css" />
        {{-- bootstrap --}}
        <!-- Scripts -->
        <script src="{{ mix('js/app.js') }}" defer></script>

        {{-- carousel landing--}}
    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.css">
    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css">

    </head>
    <body class="font-sans antialiased">
        <x-jet-banner />

        <div class="min-h-screen bg-gray-100">
            @livewire('navigation-menu')

            <!-- Page Heading -->
            @if (isset($header))
                <header class="bg-white shadow">
                    <div class="max-w-7xl mx-auto py-6 px-4 sm:px-6 lg:px-8">
                        {{ $header }}
                    </div>
                </header>
            @endif
            
            @if (request()->is('user/*') && !auth()->user()->hasRole('admin'))
                @include('layouts.usersidebar')
            @endif
            <!-- Page Content -->
            <main>
                
                
                @if (auth())
                <div class="py-5">

                </div>
                @endif

                
                @if (request()->is('user/*') && !auth()->user()->hasRole('admin'))
                <div class="h-full ml-14 mb-10 md:ml-64">
                    <div class="grid grid-cols-1 p-4 gap-4">
                        {{-- konten --}}
                        {{ $slot }}
                    </div>
                </div>
                @else
                {{ $slot }}
                @endif
            </main>
        </div>

        @stack('modals')

        @livewireScripts
        
        <script src="https://cdn.jsdelivr.net/gh/livewire/turbolinks@v0.1.x/dist/livewire-turbolinks.js" data-turbolinks-eval="false"></script>
        
        {{-- carousel landingpage--}}
        <script src="https://unpkg.com/swiper/swiper-bundle.js"></script>
        <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
        <link href="https://unpkg.com/tailwindcss@^1.0/dist/tailwind.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.min.js" defer></script>
        <link href="https://fonts.googleapis.com/css?family=Source+Code+Pro|Roboto&display=swap" rel="stylesheet">
        

    </body>
</html>
