<?php

use Illuminate\Support\Facades\Route;
use App\Http\Livewire\Home;
use App\Http\Livewire\Shop;
use App\Http\Livewire\DetailProducts;
use App\Http\Livewire\About;
use App\Http\Livewire\Katalog;
use App\Http\Livewire\Admin\Dashboard;
use App\Http\Livewire\Admin\Kategoriku;
use App\Http\Livewire\Admin\Products;
use App\Http\Livewire\Admin\FileUpload;
use App\Http\Livewire\Admin\DetailProduct;

//user
use App\Http\Livewire\User\Dsuser;
use App\Http\Livewire\User\Akun;
use App\Http\Livewire\User\Keranjang;
use App\Http\Livewire\User\Myorder;
use App\Http\Livewire\User\Setting;
use App\Http\Livewire\User\Alamat;
use App\Http\Livewire\User\AddAlamat;
use App\Http\Livewire\User\EditAlamat;
use App\Http\Livewire\User\BayarCheckout;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/homepage', function () {
    return view('homepage');
});


Route::middleware(['auth:sanctum', 'verified'], ['admin'])->get('/dashboard', Dashboard::class)
->name('dashboard');

Route::group(['middleware' => ['role:admin']], function ()
{
    Route::get('/admin/category', Kategoriku::class)->name('admin.category');
    Route::get('/admin/products', Products::class)->name('admin.products');
    Route::get('/admin/gambar', FileUpload::class);

});

Route::middleware(['auth'])->group(function () {

    Route::get('/user/dashboard', Dsuser::class)->name('user.dashboard');
    Route::get('/user/cart', Keranjang::class)->name('user.cart');
    Route::get('/user/myorder', Myorder::class)->name('user.myorder');
    Route::get('/user/profilee', Akun::class)->name('user.akun');
    Route::get('/user/setting', Setting::class)->name('user.setting');
    Route::get('/user/alamat', Alamat::class)->name('user.alamat');
    Route::get('/user/edit/{id}', EditAlamat::class)->name('user.editalamat');
    Route::get('/user/tambah-alamat', AddAlamat::class)->name('user.addalamat');
    Route::get('/user/bayar', BayarCheckout::class)->name('user.bayar');


});
Route::get('/shop/{slug}', DetailProduct::class);
Route::get('/', Home::class);
Route::get('/shop', Katalog::class);
Route::get('/about', About::class)->name('about');


Route::group(['middleware' => ['role:admin']], function () {
    return view('livewire.admin.dashboard');
});
