
    {{-- main --}}
        <div class="md:p-5">

            @if (Laravel\Fortify\Features::canUpdateProfileInformation())
            @livewire('profile.update-profile-information-form')

            @endif

        </div>