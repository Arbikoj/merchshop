<?php

namespace App\Http\Livewire\User;

use Livewire\Component;

class BayarCheckout extends Component
{
    public function render()
    {
        return view('livewire.user.bayar-checkout');
    }
}
