<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class category extends Model
{
    use HasFactory;

    protected $table = "categories";

    protected $fillable = [
        'id',
        // 'parent_id',
        'desc_category',
        'category',
        'slug', 
    ];

    public function products()
    {
        return $this->hasMany(product::class);
    }

    // public function subcategory()
    // {
    //     return $this->hasMany(subcategory::class);
    // }
    
    //slug
    protected static function boot()
    {
        parent::boot();
        static::created(function ($kategori) {
            $kategori->slug = $kategori->createSlug($kategori->category);
            $kategori->save();
        });
    }

    private function createSlug($category)
    {
        if (static::whereSlug($slug = Str::slug($category))->exists()) {

            $max = static::whereCategory($category)->latest('id')->skip(1)->value('slug');

            if (isset($max[-1]) && is_numeric($max[-1])) {

                return preg_replace_callback('/(\d+)$/', function ($mathces) {

                    return $mathces[1] + 1;
                }, $max);
            }
            return "{$slug}-2";
        }
        return $slug;
    }
    //endslug
}
