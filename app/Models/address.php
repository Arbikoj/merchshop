<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Laravolt\Indonesia\Models\Province;
use Laravolt\Indonesia\Models\City;
use Laravolt\Indonesia\Models\District;
use Laravolt\Indonesia\Models\Village;
class address extends Model
{
    use HasFactory;
    protected $table = "addresses";

    protected $fillable = [
        'id',
        'users_id',
        'province_id',
        'city_id',
        'district_id',
        'village_id',
        'nama_depan',
        'address_1',
        'address_2',
        'no_telepon',
        'post_code'
    ];
    
    public function provinsi()
    {
        return $this->belongsTo(Province::class, 'province_id');
    }

    public function kab()
    {
        return $this->belongsTo(City::class, 'city_id');
    }

    public function kec()
    {
        return $this->belongsTo(District::class, 'district_id');
    }

    public function desa()
    {
        return $this->belongsTo(Village::class, 'village_id');
    }
}
