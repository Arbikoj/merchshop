<div class="">
    {{-- A good traveler has no fixed plans and is not intent upon arriving. --}}
    {{-- ini halaman shop --}}
    <!-- component -->
    @if($isModal) @include('livewire.filter') @endif
        <div class="container mx-auto flex items-center flex-wrap pb-12">
            <nav id="store" class="w-full top-0 px-2 py-1">
                <div class="w-full container mx-auto flex items-center justify-between mt-0 px-2 py-3">
                    <a class="uppercase tracking-wide no-underline hover:no-underline font-bold text-gray-800 text-xl " href="#">
	                </a>
                    <div class="flex items-center">
                        
                </div>
                <div class="w-full flex ">
                    <div>
                        <select
                                wire:model="paginate"
                                class="appearance-none h-full rounded-l border block appearance-none bg-white border-gray-400 text-gray-700 py-2 px-1 pr-8 leading-tight focus:outline-none focus:bg-white focus:border-gray-500">
                                <option value="10">10</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                                <option value="">All</option>
                        </select>
                    </div>
                    <div>
                        <select wire:model="srckategori" class="appearance-none h-full rounded-r border block appearance-none w-full bg-white border-gray-400 text-gray-700 py-2 px-4 pr-8 leading-tight focus:outline-none focus:bg-white focus:border-gray-500">
                            <option value="">Pilih Kategori</option>
                            @foreach ($kategori as $item)
                                <option value="{{ $item->id }}">{{ $item->category }}</option>
                            @endforeach
                        </select>
                    </div>
                    
                </div>
                <div>
                    <div class="pt-2 relative mx-auto text-gray-600">
                        <input
                            wire:model="search"
                            class="shadow-lg bg-white h-10 px-5 pr-16 rounded-lg text-sm focus:outline-none"
                            type="search"
                            name="search"
                            placeholder="Search">

                            
                            <button class="absolute right-0 top-0 mt-5 mr-4">
                            <svg class="text-gray-600 h-4 w-4 fill-current" xmlns="http://www.w3.org/2000/svg"
                                xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px"
                                viewBox="0 0 56.966 56.966" style="enable-background:new 0 0 56.966 56.966;" xml:space="preserve"
                                width="512px" height="512px">
                                <path d="M55.146,51.887L41.588,37.786c3.486-4.144,5.396-9.358,5.396-14.786c0-12.682-10.318-23-23-23s-23,10.318-23,23  s10.318,23,23,23c4.761,0,9.298-1.436,13.177-4.162l13.661,14.208c0.571,0.593,1.339,0.92,2.162,0.92  c0.779,0,1.518-0.297,2.079-0.837C56.255,54.982,56.293,53.08,55.146,51.887z M23.984,6c9.374,0,17,7.626,17,17s-7.626,17-17,17  s-17-7.626-17-17S14.61,6,23.984,6z" />
                              </svg>
                            </button>
                          </div>
                    {{-- filter --}}
                    </div>
                </div>
            <div class="px-10 pt-2">{{ $katalog->links() }}</div>
<<<<<<< HEAD
=======

>>>>>>> f92cd0ed3fc2b826eaf1195547c2ee4846b7d9d8
            </nav>
            
            {{-- CONTOHH --}}
            @forelse($katalog as $item)
                
            
            @php
            $badge_diskon = $item->diskon * 100;
                $korting_diskon = $item->diskon * $item->harga;
                $harga_sekarang = $item->harga - $korting_diskon;
                $harga_sekarang =number_format( $harga_sekarang,2);

                $harga =number_format( $item->harga,2);
                $produk_skr = (str_word_count($item->nama_produk) > 9 ? substr($item->nama_produk,0,60).".." : $item->nama_produk );
               

                if(str_word_count($item->nama_produk) > 60){
                    substr("isi tulisan artikel",0,200)."[..]"
                }
                 "isi tulisan artikel")
                
            @endphp
            
            {{-- katalog --}}
            <div class="w-full rounded md:w-1/3 xl:w-1/4 p-3 flex flex-col">
                <a href="/shop/{{$item->slug}}">
                    @if ($item->diskon != 0)
                    <div class="bg-yellow-400 absolute mt-8 p-2">
                            <div class="block text-red-700 font-medium text-bold">{{ $badge_diskon }}%</div>
                            <div class="block text-gray-800 font-medium text-semibold">Sale</div>
                    </div>
                    @endif
                    @if ($item->pre_order == 1)
                        <div class="bg-gray-300 rounded-md absolute mt-48 p-1">
                            <div class="block text-gray-700 font-medium text-bold">preorder</div>
                        </div>
                    @endif
                    
                    <div class="hover:shadow-2xl shadow duration-700 bg-white p-3 h-96">
                        
                        <img class="hover:grow h-4/6 w-auto object-cover flex mx-auto object-center" src="storage/{{$item->photos}}">
                        <p class="text-md font-bold title-font text-uppercase text-indigo-500 tracking-widest">{{$item->kategori->category}}</p>
                        <div class="pt-21flex items-center justify-between">
<<<<<<< HEAD
                            <p class="font-medium text-semibold">{{$item->nama_produk}}...</p>
=======

                            <p class="font-medium text-semibold">{{$produk_skr}}</p>

>>>>>>> 16186f5966093b5241c130fbf830a1a0b8565449
                        </div>
                        <div class="font-medium flex items-center justify-between">
                            @if ($harga != $harga_sekarang)
                                <p class="line-through pt-1 text-red-300">Rp.{{$harga}}</p>
                                <p class="pt-1 text-accent">Rp.{{$harga_sekarang}}</p>
                            @else
                                <p class="pt-1 text-accent">Rp.{{$harga}}</p>

                            @endif
                            
                            <svg class="h-8 w-8 fill-current text-gray-500 hover:text-black duration-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                                <path d="M12,4.595c-1.104-1.006-2.512-1.558-3.996-1.558c-1.578,0-3.072,0.623-4.213,1.758c-2.353,2.363-2.352,6.059,0.002,8.412 l7.332,7.332c0.17,0.299,0.498,0.492,0.875,0.492c0.322,0,0.609-0.163,0.792-0.409l7.415-7.415 c2.354-2.354,2.354-6.049-0.002-8.416c-1.137-1.131-2.631-1.754-4.209-1.754C14.513,3.037,13.104,3.589,12,4.595z M18.791,6.205 c1.563,1.571,1.564,4.025,0.002,5.588L12,18.586l-6.793-6.793C3.645,10.23,3.646,7.776,5.205,6.209 c0.76-0.756,1.754-1.172,2.799-1.172s2.035,0.416,2.789,1.17l0.5,0.5c0.391,0.391,1.023,0.391,1.414,0l0.5-0.5 C14.719,4.698,17.281,4.702,18.791,6.205z" />
                            </svg>
                        </div>
                        
                    </div>
                </a>
            </div>
            @empty
            <div class="flex mx-auto text-2xl font-semibold">

                Tidak ada Produk
            </div>
            @endforelse ($katalog as $item)
<<<<<<< HEAD
        </div>            
=======
        </div>
            
>>>>>>> f92cd0ed3fc2b826eaf1195547c2ee4846b7d9d8
</div>
