<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use App\Models\category;
use App\Models\subcategory;
use Livewire\WithPagination;


class Kategoriku extends Component
{
    public  $category_id,
            $category,
            $slug,
            $desc_category;
    //pagination default
    public $paginate = 10;

    public $validateDelete = 0;
    //variable search
    public $srcItem;

    public $subkategori, $key, $value, $parent_id;
    use WithPagination;
    public function render()
    {
        $srcItem = '%'.$this->srcItem . '%';

        return view('livewire.admin.kategoriku',[
            'kategori' => category::where('category','LIKE', $srcItem)
            ->orWhere('desc_category','LIKE',$srcItem)
            ->orderBy('created_at', 'DESC')->paginate($this->paginate),
        ]);
    }

    public function resetFields()
    {
    
        $this->category_id = '';
        $this->category = '';
        $this->desc_category = '';
        $this->slug = '';
    }
    //METHOD STORE AKAN MENG-HANDLE FUNGSI UNTUK MENYIMPAN / UPDATE DATA
    public function store()
    {
        //MEMBUAT VALIDASI
        $this->validate([
            'category' => 'required',
            'desc_category' => 'required',
            // 'slug' => 'required',
        ]);

        //QUERY UNTUK MENYIMPAN / MEMPERBAHARUI DATA MENGGUNAKAN UPDATEORCREATE
        //DIMANA ID MENJADI UNIQUE ID, JIKA IDNYA TERSEDIA, MAKA UPDATE DATANYA
        //JIKA TIDAK, MAKA TAMBAHKAN DATA BARU
        category::updateOrCreate(['id' => $this->category_id], [
    
            'category' => $this->category,
            'desc_category' => $this->desc_category,
            'slug' => $this->slug,
        ]);

        //BUAT FLASH SESSION UNTUK MENAMPILKAN ALERT NOTIFIKASI
        session()->flash('message', $this->category_id ? $this->category . ' Diperbaharui': $this->category . ' Ditambahkan');
        $this->resetFields();
        return redirect('/admin/category');

    }

    //FUNGSI INI UNTUK MENGAMBIL DATA DARI DATABASE BERDASARKAN ID MEMBER
    public function edit($id)
    {
        $produk = category::find($id); //BUAT QUERY UTK PENGAMBILAN DATA
        //LALU ASSIGN KE DALAM MASING-MASING PROPERTI DATANYA
        $this->category_id = $id;

        // $this->subcategories_id = $produk->subcategories_id;
        $this->category = $produk->category;
        $this->desc_category = $produk->desc_category;
        $this->slug = $produk->slug;

    }

    public function delete($id)
    {
        $produk = category::find($id); //BUAT QUERY UNTUK MENGAMBIL DATA BERDASARKAN ID
        $produk->delete(); //LALU HAPUS DATA
        session()->flash('message', $produk->category . ' Dihapus'); //DAN BUAT FLASH MESSAGE UNTUK NOTIFIKASI
        return redirect()->route('admin.category');
    }

    public function validateDelete()
    {
        $this->validateDelete = true;
    }

    public function closeDelete()
    {
        $this->validateDelete = false;
    }

}
