<!--Nav-->
<head>
    <link
        href="https://unpkg.com/tailwindcss/dist/tailwind.min.css"
        rel="stylesheet">

        <!--Replace with your tailwind.css once created-->
        <link
            href="https://fonts.googleapis.com/css?family=Work+Sans:200,400&display=swap"
            rel="stylesheet"></head>

        {{-- ANCHOR Login modal --}}
        <input type="checkbox" id="login-modal" class="modal-toggle"> 
            <div class="modal bg-gray-800 bg-transparent opacity-70">
                <div class="modal-box bg-gray-100">
                    <div class="font-bold text-gray-700 py-3">LOGIN</div>
                    @if (session('status'))
                    <div class="mb-4 font-medium text-sm text-green-600">
                        {{ session('status') }}
                    </div>
                    @endif

                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div>
                            <x-jet-label for="email" value="{{ __('Email') }}"/>
                            <x-jet-input
                                id="email"
                                class="block mt-1 w-full"
                                type="email"
                                name="email"
                                :value="old('email')"
                                required="required"
                                autofocus="autofocus"/>
                        </div>

                        <div class="mt-4">
                            <x-jet-label for="password" value="{{ __('Password') }}"/>
                            <x-jet-input
                                id="password"
                                class="block mt-1 w-full"
                                type="password"
                                name="password"
                                required="required"
                                autocomplete="current-password"/>
                        </div>

                        <div class="block mt-4">
                            <label for="remember_me" class="flex items-center">
                                <x-jet-checkbox id="remember_me" name="remember"/>
                                <span class="ml-2 text-sm text-gray-600">{{ __('Remember me') }}</span>
                            </label>
                        </div>

                        <div class="flex items-center justify-end mt-4">
                            @if (Route::has('password.request'))
                            <a
                                class="underline text-sm text-gray-600 hover:text-gray-900"
                                href="{{ route('password.request') }}">
                                {{ __('Forgot your password?') }}
                            </a>
                            @endif

                            <button
                                href="route('login')"
                                active="request()->routeIs('login')"
                                class="btn px-2 mx-2 btn-outline btn-sm btn-accent">Login</button>
                                <div class="bg-red-400 rounded-lg">
                                    <label for="login-modal" class="btn px-2 btn-error btn-outline btn-sm">Close</label>
                                </div>
                        </div>
                        <a
                                class="underline text-sm text-gray-600 hover:text-gray-900"
                                href="{{ route('register') }}">
                                {{ __("Don't have an account? Sign Up!") }}
                            </a>
                    </form>
                    <div class="modal-action"></div>
                </div>
            </div>
            {{-- ANCHOR end login modal --}}

            <nav id="header" class="fixed w-full bg-accent z-30 top-0 py-1">
                <div class="w-full container mx-auto flex flex-wrap items-center justify-between mt-0 px-6">
                    
                    {{-- <div for="menu-toggle" class="cursor-pointer md:hidden block">
                        <svg
                            class="fill-current text-gray-900"
                            xmlns="http://www.w3.org/2000/svg"
                            width="20"
                            height="20"
                            viewBox="0 0 20 20">
                            <title>menu</title>
                            <path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z"></path>
                        </svg>
                    </div> --}}

                    <input class="hidden" type="checkbox" id="menu-toggle"/>
                    <div class=" cursor-pointer md:hidden block">
                            <x-jet-dropdown align="left" width="48">
                                <x-slot name="trigger">
                                    <span class="inline-flex rounded-md">
                                        <button
                                            type="button"
                                            class="inline-flex items-center px-3 py-2 border border-transparent text-sm leading-4 font-medium rounded-md text-gray-500 hover:text-gray-700 focus:outline-none transition">
                                            <svg
                                                class="fill-current text-gray-900"
                                                xmlns="http://www.w3.org/2000/svg"
                                                width="20"
                                                height="20"
                                                viewBox="0 0 20 20">
                                                <title>menu</title>
                                                <path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z"></path>
                                            </svg>
                                        </button>
                                    </span>
                                </x-slot>

                                <x-slot name="content">
                                    <x-jet-dropdown-link href="/shop">
                                        {{ __('Shop') }}
                                    </x-jet-dropdown-link>

                                    <x-jet-dropdown-link href="/about">
                                        {{ __('About') }}
                                    </x-jet-dropdown-link>
                                </x-slot>
                            </x-jet-dropdown>
                        </div>
                    <div
                        class="hidden md:flex md:items-center md:w-auto w-full order-3 md:order-1"
                        id="menu">
                        <nav>
                            <ul
                                class="md:flex items-center justify-between text-base text-gray-700 pt-4 md:pt-0">
                                <li>
                                    <a
                                        class="inline-block no-underline hover:text-black hover:underline py-2 px-4"
                                        href="/shop">Shop</a>
                                </li>
                                <li>
                                    <a
                                        class="inline-block no-underline hover:text-black hover:underline py-2 px-4"
                                        href="/about">About</a>
                                </li>

                            </ul>
                        </nav>

                    </div>
                    

                    <div class="order-1 md:order-2">
                        <a
                            href="/"
                            class="flex items-center tracking-wide no-underline hover:no-underline font-bold text-gray-800 text-xl ">
                            <svg
                                class="fill-current text-gray-800 mr-2"
                                xmlns="http://www.w3.org/2000/svg"
                                width="24"
                                height="24"
                                viewBox="0 0 24 24">
                                <path
                                    d="M5,22h14c1.103,0,2-0.897,2-2V9c0-0.553-0.447-1-1-1h-3V7c0-2.757-2.243-5-5-5S7,4.243,7,7v1H4C3.447,8,3,8.447,3,9v11 C3,21.103,3.897,22,5,22z M9,7c0-1.654,1.346-3,3-3s3,1.346,3,3v1H9V7z M5,10h2v2h2v-2h6v2h2v-2h2l0.002,10H5V10z"/>
                            </svg>
                            MerchShop
                        </a>
                    </div>
                    {{-- cart icon --}}
                    @livewire('cart-icon')

                </div>
            </nav>

            <!-- AlpineJS -->
<script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.min.js" defer></script>
<!-- Font Awesome -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js" integrity="sha256-KzZiKy0DWYsnwMF+X1DvQngQ2/FxF7MF3Ff72XcpuPs=" crossorigin="anonymous"></script>