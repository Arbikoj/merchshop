<div>
    {{-- A good traveler has no fixed plans and is not intent upon arriving. --}}

    @if (session()->has('message'))
            <div
                class="bg-green-100 border-t-4 border-green-500 rounded-b text-green-900 px-4 py-3 shadow-md my-3"
                role="alert">
                <div class="flex">
                    <div>
                        <p class="text-sm">{{ session('message') }}</p>
                    </div>
                </div>
            </div>
            @endif
    <button class="'w-auto bg-blue-500 hover:bg-blue-700 rounded-lg shadow-xl font-medium text-white px-4 py-2"><a href="{{route('user.addalamat')}}">Tambah Alamat</button></a> 

    <dir class="bg-gray-200 p-0 w-full rounded-md">
        @foreach ($alamat as $item)
        <a href="/user/edit/{{ $item->id }}">
        <div class="bg-white m-2 p-3">
            <p class="text-xl font-semibold">{{$item->nama_depan}}</p>
            <p class="text-md font-normal">{{$item->no_telepon}}</p>
            <p class="text-md font-normal">{{$item->address_1}} {{$item->address_2}}</p>
            <p class="text-md font-normal capitalize">{{$item->kab->name}} - {{$item->kec->name}}, {{$item->provinsi->name}}, {{ $item->post_code }}</p>

            
        </div>
    </a>
    @endforeach
    </dir>
    
    
</div>
