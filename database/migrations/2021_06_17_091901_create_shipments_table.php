<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShipmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('orders_id')->constrained();
            $table->foreignId('detail_users_id')->constrained();
            $table->decimal('total_berat');
            $table->decimal('total_harga');
            $table->string('shipped_by', 50);
            $table->dateTime('shipped_at', $precision = 0);
            $table->timestamp('deleted_at', $precision = 0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipments');
    }
}
