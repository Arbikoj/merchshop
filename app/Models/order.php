<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class order extends Model
{
    use HasFactory;
    protected $table = "member";

    protected $fillable = [
        'id',
        'detail_users_id',
        'order_date',
        'status_pembayaran',
        'token_pembayaran',
        'url_pembayaran',
        'harga_dasar',
        'ongkir',
        'subtotal_ongkir',
        'subtotal_produk',
        'total_harga',
        'opsi_pengiriman',
        'catatan',
        'approved_by',
        'approved_at',
        'cancel_by',
        'canceled_at',
        'catatan_pembatalan'

    ];
}
