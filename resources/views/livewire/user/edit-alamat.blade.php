<div>
    {{-- Care about people's approval and you will be their prisoner. --}}

    {{-- breadcrumbs --}}
    <nav class="bg-gray-300 p-3 rounded font-sans w-full m-3">
        <ol class="list-reset flex text-grey-dark">
            <li><a href="#" class="text-blue-400 font-bold">Dashboard</a></li>
            <li><span class="mx-2">/</span></li>
            <li><a href="#" class="text-blue-400 font-bold">Alamat</a></li>
            <li><span class="mx-2">/</span></li>
            <li>Tambah Alamat</li>
        </ol>
    </nav>
    {{$users_id}}
    {{-- <input
    id="users_id"
    type="text"
    wire:model="users_id"
    autocomplete="users_id"
    placeholder="users"
    class="w-full py-2 px-3 rounded-lg border-2 border-blue-300 mt-1 focus:outline-none focus:ring-2 focus:ring-blue-600 focus:border-transparent"/> --}}

    <div class="pl-5 flex mx-auto text-3xl font-semibold">
        <div class="flex">Tambah ALamat</div>
    </div>
    {{-- nama penerima --}}
    <div class="grid grid-cols-1 mx-7 mt-5">
        <x-jet-label for="name" value="{{ __('Nama') }}"/>
        <input
            id="name"
            type="text"
            wire:model="nama_depan"
            autocomplete="nama_depan"
            placeholder="Nama Penerima"
            class="w-full py-2 px-3 rounded-lg border-2 border-blue-300 mt-1 focus:outline-none focus:ring-2 focus:ring-blue-600 focus:border-transparent"/>
        <x-jet-input-error for="nama_depan" class="mt-2"/>
    </div>
    <div class="flex grid grid-cols-1 md:grid-cols-2 gap-5 md:gap-8 mt-5 mx-7">
        <!-- kodepos -->
            <div class="grid grid-cols-1">
                <x-jet-label for="name" value="{{ __('Kode Pos') }}"/>
                <input
                    id="post_code"
                    type="text"
                    wire:model="post_code"
                    autocomplete="post_code"
                    placeholder="Kode Pos"
                    class="w-full py-2 px-3 rounded-lg border-2 border-blue-300 mt-1 focus:outline-none focus:ring-2 focus:ring-blue-600 focus:border-transparent"/>
                <x-jet-input-error for="post_code" class="mt-2"/>
            </div>

            <!-- No hp -->
            <div class="grid grid-cols-1">
                <x-jet-label for="no_telepon" value="{{ __('No. Telp') }}"/>
                <x-jet-input
                    id="no_telepon"
                    type="text"
                    placeholder="No Penerima"
                    wire:model="no_telepon"
                    class="w-full py-2 px-3 rounded-lg border-2 border-blue-300 mt-1 focus:outline-none focus:ring-2 focus:ring-blue-600 focus:border-transparent"/>
                <x-jet-input-error for="no_telepon" class="mt-2"/>
            </div>
        </div>
        <div class="grid grid-cols-1 md:grid-cols-2 gap-5 md:gap-8 mt-5 mx-7">
            {{-- Provinsi --}}
            <div class="grid grid-cols-1">
                <x-jet-label for="jenis_kelamin" value="{{ __('Provinsi') }}"/>
                {{$province_id}}
                <select
                    id="province_id"
                    name="province_id"
                    wire:model="province_id"
                    class="py-2 px-3 rounded-lg border-2 border-blue-300 mt-1 focus:outline-none focus:ring-2 focus:ring-blue-600 focus:border-transparent">
                    <option>Pilih Provinsi</option>
                    @foreach ($provinces as $id => $name)
                    <option value="{{ $id }}">{{ $name }}</option>
                    @endforeach
                </select>
                <x-jet-input-error for="province_id" class="mt-2"/>

            </div>
            {{-- Kabupaten/Kota --}}
            <div class="grid grid-cols-1">
                <x-jet-label for="jenis_kelamin" value="{{ __('Kabupaten/Kota') }}"/>
                {{$city_id}}

                <select
                id="city"
                name="city"
                wire:model="city_id"
                class="py-2 px-3 rounded-lg border-2 border-blue-300 mt-1 focus:outline-none focus:ring-2 focus:ring-blue-600 focus:border-transparent">
                <option>Pilih Kabupaten/Kota</option>
                @foreach ($cities as $kota)
                <option value="{{ $kota->id }}">{{ $kota->name }}</option>
                @endforeach
        </select>
            <x-jet-input-error for="city_id" class="mt-2"/>
            </div>
        </div>
        <div class="grid grid-cols-1 md:grid-cols-2 gap-5 md:gap-8 mt-5 mx-7">
            {{-- kecamatan --}}
            <div class="grid grid-cols-1">
                <x-jet-label for="district_id" value="{{ __('Kecamatan') }}"/>
                {{$district_id}}
                <select
                    id="district"
                    name="district"
                    wire:model="district_id"
                    class="py-2 px-3 rounded-lg border-2 border-blue-300 mt-1 focus:outline-none focus:ring-2 focus:ring-blue-600 focus:border-transparent">
                    <option>Pilih Kecamatan</option>
                    @foreach ($district as $kec)
                    <option value="{{ $kec->id }}">{{ $kec->name }}</option>
                    @endforeach

                </select>
                <x-jet-input-error for="district_id" class="mt-2"/>

            </div>
            {{-- desa --}}
            <div class="grid grid-cols-1">
                <x-jet-label for="jenis_kelamin" value="{{ __('Desa/Kelurahan') }}"/>
                {{$village_id}}
                <select
                id="village"
                name="village"
                wire:model="village_id"
                class="py-2 px-3 rounded-lg border-2 border-blue-300 mt-1 focus:outline-none focus:ring-2 focus:ring-blue-600 focus:border-transparent">
                <option>Pilih desa/kelurahan</option>
                @foreach ($village as $desa)
                <option value="{{ $desa->id }}">{{ $desa->name }}</option>
                @endforeach

            </select>
            <x-jet-input-error for="village_id" class="mt-2"/>
            </div>
        </div>
        <div class="grid grid-cols-1 md:grid-cols-2 gap-5 md:gap-8 mt-5 mx-7">
            {{-- alamat 1 --}}
            <div class="grid grid-cols-1">
                <x-jet-label for="address_1" value="{{ __('Alamat 1') }}"/>
                <x-jet-input
                    id="addres_1"
                    type="text"
                    placeholder="Nama Jalan, gedung, rumah dll"
                    wire:model="address_1"
                    class="w-full py-2 px-3 rounded-lg border-2 border-blue-300 mt-1 focus:outline-none focus:ring-2 focus:ring-blue-600 focus:border-transparent"/>
                <x-jet-input-error for="addres_1" class="mt-2"/>

            </div>
            {{-- Alamat 2 --}}
            <div class="grid grid-cols-1">
                <x-jet-label for="address_2" value="{{ __('Alamat 2') }}"/>
                <x-jet-input
                    id="addres_2"
                    type="text"
                    wire:model="address_2"
                    placeholder="Gg, No.rumah, Blok, Unit, patokan dll"
                    class="w-full py-2 px-3 rounded-lg border-2 border-blue-300 mt-1 focus:outline-none focus:ring-2 focus:ring-blue-600 focus:border-transparent"/>
            <x-jet-input-error for="addres_2" class="mt-2"/>
            </div>
        </div>

        {{-- tombol --}}
        <div class='flex items-center justify-end  md:gap-8 gap-4 pt-5 pr-8 pb-5'>
            <button
                wire:click="batal()"
                type="button"
                class='w-auto bg-gray-500 hover:bg-gray-700 rounded-lg shadow-xl font-medium text-white px-4 py-2'>Batal</button>
            <button
                wire:click.prevent="update()"
                class='w-auto bg-blue-500 hover:bg-blue-700 rounded-lg shadow-xl font-medium text-white px-4 py-2'>
                Simpan

            </button>
            <button
                wire:click="delete({{ $alamat_id }})"
                class='w-auto bg-red-500 hover:bg-red-700 rounded-lg shadow-xl font-medium text-white px-4 py-2'>
                Hapus Alamat

            </button>
        </div>
</div>
