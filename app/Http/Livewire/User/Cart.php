<?php

namespace App\Http\Livewire\User;

use Livewire\Component;

class Cart extends Component
{
    public $cart;

    public function mount()
    {
        $this->cart = Cart::get();
    }

    public function render()
    {
        return view('livewire.user.cart');
    }

    public function removeItem()
    {
        // dd($productId);
        // Cart::removed($productId);
        // $this->cart = Cart::get();
        // $this->emit('productRemoved');   
        return redirect()->route('about');    
    }
    public function checkout()
    {
        Cart::clear();
        $this->emit('clearCart');
        $this->cart = Cart::get();
    }
}
