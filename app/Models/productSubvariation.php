<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class productSubvariation extends Model
{
    use HasFactory;
    
    protected $table = "product_subvariations";

    protected $fillable = [
            'id',
            'subvariation', 
    ];
}
