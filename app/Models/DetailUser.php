<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetailUser extends Model
{
    use HasFactory;
    protected $table = "detail_users";

    protected $fillable = [
        'id',
        'users_id',
        'jenis_kelamin',
        'tanggal_lahir',
        'nama_depan',
        'nama_belakang',
        'email_verivication',
        'no_telepon',
        'password',
        'confirm_password'
    ];
}
