<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePembayaransTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pembayarans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('orders_id')->constrained();
            $table->foreignId('detail_users_id')->constrained();
            $table->decimal('number');
            $table->decimal('amount');
            $table->string('status', 225);
            $table->string('token', 225);
            $table->json('payloads');
            $table->string('payment_type', 225);
            $table->string('va_number', 225);
            $table->string('vendor_name', 225);
            $table->string('biller_code', 225);
            $table->string('biller_key', 225);
            $table->string('method', 225);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pembayarans');
    }
}
