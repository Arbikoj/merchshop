<?php

namespace App\Http\Livewire\User;

use Livewire\Component;
use App\Facades\Cart;
class Myorder extends Component
{
    public $cart;

    public function mount()
    {
        $this->cart = Cart::get();
    }

    public function render()
    {
        return view('livewire.user.myorder');
    }

    public function updateCartTotal()
    {
        $this->cartTotal = count(Cart::get()['product']);
    }

    public function removeItem($productId)
    {
        // dd($productId);
        Cart::remove($productId);
        $this->cart = Cart::get();
        $this->emit('productRemoved');   
    }
}
