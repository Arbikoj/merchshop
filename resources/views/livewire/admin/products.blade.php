<div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
<div class="block bg-white overflow-hidden shadow-xl sm:rounded-lg px-4 ">
        @if (session()->has('message'))
            <div
                class="bg-green-100 border-t-4 border-green-500 rounded-b text-green-900 px-4 py-3 shadow-md my-3"
                role="alert">
                <div class="flex">
                    <div>
                        <p class="text-sm">{{ session('message') }}</p>
                    </div>
                </div>
            </div>
            @endif @if($isModal) @include('livewire.admin.add-products') @endif
                {{-- tambah produk --}}
                        <div>
                            <button
                                wire:click="create()"
                                class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded my-3">Tambah Produk</button>
                        </div>
                        
                        <div>
                            <h2 class="text-2xl font-semibold leading-tight">produk</h2>
                        </div>
                        <div class="block my-2 flex sm:flex-row flex-col">
                            <div class="flex flex-row mb-1 sm:mb-0">
                                <div class="relative">
                                    <select
                                        wire:model="paginate"
                                        class="appearance-none h-full rounded-l border block appearance-none w-full bg-white border-gray-400 text-gray-700 py-2 px-4 pr-8 leading-tight focus:outline-none focus:bg-white focus:border-gray-500">
                                        <option value="10">10</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                        <option value="">All</option>
                                    </select>
                                    <div
                                        class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                                        {{-- <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                            <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
                                        </svg> --}}
                                    </div>
                                </div>
                                {{-- <div class="relative">
                                    <select
                                        class=" h-full rounded-r border-t sm:rounded-r-none sm:border-r-0 border-r border-b block appearance-none w-full bg-white border-gray-400 text-gray-700 py-2 px-4 pr-8 leading-tight focus:outline-none focus:border-l focus:border-r focus:bg-white focus:border-gray-500">
                                        <option>All</option>
                                        <option>Active</option>
                                        <option>Inactive</option>
                                    </select>
                                    <div
                                        class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                                        {{-- <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                            <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
                                        </svg> --}}
                                    {{-- </div> --}}
                                {{-- </div> --}}
                            </div>
                            <div class="block relative">
                                <span class="h-full absolute inset-y-0 left-0 flex items-center pl-2">
                                    <svg viewBox="0 0 24 24" class="h-4 w-4 fill-current text-gray-500">
                                        <path
                                            d="M10 4a6 6 0 100 12 6 6 0 000-12zm-8 6a8 8 0 1114.32 4.906l5.387 5.387a1 1 0 01-1.414 1.414l-5.387-5.387A8 8 0 012 10z"></path>
                                    </svg>
                                </span>
                                <input
                                    placeholder="Search"
                                    wire:model="srcItem"
                                    class="appearance-none rounded-r rounded-l sm:rounded-l-none border border-gray-400 border-b block pl-8 pr-6 py-2 w-full bg-white text-sm placeholder-gray-400 text-gray-700 focus:bg-white focus:placeholder-gray-600 focus:text-gray-700 focus:outline-none"/>
                            </div>
                        </div>
                        <div class="-mx-4 sm:-mx-8 px-4 sm:px-8 py-4 overflow-x-auto">
                            <div class="inline-block min-w-full shadow rounded-lg overflow-hidden">
                                <table class="min-w-full leading-normal">
                                    <thead>
                                        <tr>
                                            <th
                                                class="w-5  px-2 py-3 justify-center border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                                                <span class="mx-auto font-bold justify-center">Opsi</span>
                                            </th>
                                            <th
                                                class="mx-auto px-2 py-3 justify-center border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                                                <span class="font-bold justify-center">Nama Produk</span>
                                            </th>
                                            <th
                                                class="mx-auto px-2 py-3 justify-center border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                                                <span class="font-bold justify-center">Desc</span>
                                            </th>
                                            <th
                                                class="mx-auto px-2 py-3 justify-center border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                                                <span class="font-bold justify-center">status</span>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse($katalog as $row)
                                       <tr>
                                            <td class="px-2 py-2 md:px-5 md:py-5 md:text-lg text-md border-b border-gray-200 bg-white">
                                                <div class="flex justify-around">

                                                    {{-- show details --}}
                                                    <a href="/shop/{{$row->slug}}">
                                                        <button class="p-1 text-red-600 hover:bg-red-600 hover:text-white rounded mx-0">
                                                            <svg
                                                                class="w-5 h-5"
                                                                fill="currentColor"
                                                                viewBox="0 0 20 20"
                                                                xmlns="http://www.w3.org/2000/svg">
                                                                <path d="M10 12a2 2 0 100-4 2 2 0 000 4z"></path>
                                                                <path
                                                                    fill-rule="evenodd"
                                                                    d="M.458 10C1.732 5.943 5.522 3 10 3s8.268 2.943 9.542 7c-1.274 4.057-5.064 7-9.542 7S1.732 14.057.458 10zM14 10a4 4 0 11-8 0 4 4 0 018 0z"
                                                                    clip-rule="evenodd"></path>
                                                            </svg>
                                                        </button>
                                                    </a>
                                                    {{-- edit --}}
                                                    <button
                                                        wire:click="edit({{ $row->id }})"
                                                        class="p-1 text-red-600 hover:bg-red-600 hover:text-white rounded mx-0">
                                                        <svg
                                                            class="w-5 h-5"
                                                            fill="black"
                                                            viewBox="0 0 20 20"
                                                            xmlns="http://www.w3.org/2000/svg">
                                                            <path
                                                                d="M13.586 3.586a2 2 0 112.828 2.828l-.793.793-2.828-2.828.793-.793zM11.379 5.793L3 14.172V17h2.828l8.38-8.379-2.83-2.828z"></path>
                                                        </svg>
                                                    </button>
                                                    {{-- delete --}}
                                                    <button
                                                        wire:click="delete({{ $row->id }})"
                                                        class="p-1 text-red-600 hover:bg-red-600 hover:text-white rounded mx-0">
                                                        <svg
                                                            class="w-5 h-5"
                                                            fill="currentColor"
                                                            viewBox="0 0 20 20"
                                                            xmlns="http://www.w3.org/2000/svg">
                                                            <path
                                                                fill-rule="evenodd"
                                                                d="M9 2a1 1 0 00-.894.553L7.382 4H4a1 1 0 000 2v10a2 2 0 002 2h8a2 2 0 002-2V6a1 1 0 100-2h-3.382l-.724-1.447A1 1 0 0011 2H9zM7 8a1 1 0 012 0v6a1 1 0 11-2 0V8zm5-1a1 1 0 00-1 1v6a1 1 0 102 0V8a1 1 0 00-1-1z"
                                                                clip-rule="evenodd"></path>
                                                        </svg>
                                                    </button>
                                                    {{-- modal delete --}}

                                            </td>
                                            <td class="px-2 py-2 md:px-5 md:py-5 md:text-lg border-b border-gray-200 bg-white text-md">
                                                    <div class="flex items-center">

                                                        <div class="ml-3">
                                                            <p class="text-gray-900 whitespace-no-wrap">
                                                                {{ $row->nama_produk }}
                                                            </p>
                                                        </div>
                                                    </div>
                                            </td>
                                            
                                            <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                                                    <p class="text-gray-900 whitespace-no-wrap">
                                                        {{ $row->desc_produk }}
                                                    </p>
                                            </td>
                                           <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                                                        <div class="form-control">
                                                          <label class="cursor-pointer label">
                                                            <div>
                                                                {{-- <input wire:model="release_product" type="checkbox" name="toggle" id="toggle" class="toggle-checkbox absolute block w-6 h-6 rounded-full bg-white border-4 appearance-none cursor-pointer"/> --}}
                                                                <input wire:click="statusrelease({{ $row->id }})" wire:model="release_product" type="checkbox" checked="checked" class="toggle toggle-accent"> 
                                                                <span value="10" class="toggle-mark"></span>
                                                            </div>
                                                            {{$release_product}}
                                                          </label>
                                                        </div>
                                                </td> 
                                            {{-- <p>{{ $row->release_product}}</p> --}}
                                                
                                        </tr>

                                        @empty
                                        <tr>
                                            <td class="border px-4 py-2 text-center text-md font-bold" colspan="5">Tidak ada data</td>
                                        </tr>
                                        @endforelse
                                    </tbody>

                                </table>

                            </div>
                        </div>
                    <div class="px-10 pt-2">{{ $katalog->links() }}</div>
        </div>
        </div>
    </div>
</div>
