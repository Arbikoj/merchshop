<?php

namespace App\Http\Livewire\User;

use Livewire\Component;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

use App\Models\address;

use Laravolt\Indonesia\Models\Province;
use Laravolt\Indonesia\Models\City;
use Laravolt\Indonesia\Models\District;
use Laravolt\Indonesia\Models\Village;

class AddAlamat extends Component
{
    public $id_akun, $users, $coba;
    public
        $alamat_id,
        $users_id,
        $province_id,
        $city_id,
        $district_id,
        $village_id,
        $nama_depan,
        $address_1,
        $address_2,
        $no_telepon,
        $post_code;
    //var fungsi
    public $provinces, $cities, $district, $village;

    public function render()
    {
        $this->id_akun = Auth::id();

        
        $this->opsiKelurahan($this->district_id);
        $this->opsiKecamatan($this->city_id);
        $this->opsiKota($this->province_id);

        
        $this->provinces = Province::pluck('name', 'id');
        // $this->cities = City::pluck('province_id',$id_prov)->get();

        return view('livewire.user.add-alamat');
    }

    public function opsiKota($id_prov)
    {
        $this->cities = City::where('province_id',$id_prov)->get();
    }
    public function opsiKecamatan($id_kec)
    {
        $this->district = District::where('city_id',$id_kec)->get();
    }
    public function opsiKelurahan($id_kel)
    {
        $this->village = Village::where('district_id',$id_kel)->get();
    }


    public function resetFields()
    {
        $this->users_id ='';
        $this->alamat_id = '';
        $this->province_id = '';
        $this->city_id = '';
        $this->district_id= '';
        $this->village_id= '';
        $this->nama_depan= '';
        $this->address_1= '';
        $this->address_2= '';
        $this->post_code= '';
        $this->no_telepon= '';
    }

    // tambah alamat
    public function add()
    {
        $rules = [
            'province_id' => 'required|string',
            'city_id' => 'required',
            'district_id' => 'required',
            'village_id' => 'required',
            'nama_depan' => 'required',
            'address_1' => 'required',
            'address_2'  => 'required',
            'post_code'  => 'required',
            'no_telepon'=> 'required',
        ];
        $this->validate($rules);

        address::updateOrCreate(['id' => $this->alamat_id], [
            'users_id' => $this->id_akun,
            'province_id' => $this->province_id,
            'city_id' => $this->city_id,
            'district_id' => $this->district_id,
            'village_id' => $this->village_id,
            'nama_depan' => $this->nama_depan,
            'address_1' => $this->address_1,
            'address_2' => $this->address_2,
            'post_code'  => $this->post_code,
            'no_telepon'  => $this->no_telepon,
        ]);

        //BUAT FLASH SESSION UNTUK MENAMPILKAN ALERT NOTIFIKASI
        session()->flash('message', $this->alamat_id ? $this->nama_depan . ' Diperbaharui': $this->nama_depan . ' Ditambahkan');
        $this->resetFields(); //DAN BERSIHKAN FIELD
        return redirect()->route('user.alamat');
    }
    
    public function batal()
    {
        $this->resetFields();
        return redirect()->route('user.alamat');

    }
    
}
