<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;

class Dashboard extends Component
{
    public function render()
    {
        if (auth()->user()->hasRole('admin')) {
            return view('livewire.admin.dashboard');
        } else {
            return view('livewire.user.dsuser');
        }
        
    }
}
