require('./bootstrap');

require('alpinejs');

// turbolinks
var Turbolinks = require("turbolinks")
Turbolinks.start()

// sweetalert delete

import Swal from 'sweetalert2';

window.deleteConfirm = function(formId) {
    Swal.fire({
        icon: 'warning',
        text: 'Do you want to delete this?',
        showCancelButton: true,
        confirmButtonText: 'Delete',
        confirmButtonColor: '#e3342f',
    }).then((result) => {
        if (result.isConfirmed) {
            document.getElementById(formId).submit();
        }
    });
}