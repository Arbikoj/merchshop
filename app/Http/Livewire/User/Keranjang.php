<?php

namespace App\Http\Livewire\User;

use Livewire\Component;
use App\Facades\Cart;

class Keranjang extends Component
{
    public $cart;
    public $jum_item = 3;
    public function mount()
    {
        $this->cart = Cart::get();
    }

    public function render()
    {
        return view('livewire.user.keranjang');
    }

    
    public function updateCartTotal()
    {
        $this->cartTotal = count(Cart::get()['product']);
    }

    public function removeItem($productId)
    {
        // dd($productId);
        Cart::remove($productId);
        $this->cart = Cart::get();
        $this->emit('productRemoved');   
    }
    public function checkout()
    {
        Cart::clear();
        $this->emit('clearCart');
        $this->cart = Cart::get();
        return redirect()->route('user.bayar');
    }
}
